<!DOCTYPE html>
<html lang="en-US">

<head><title>Processing..</title>
    <script language="javascript">
        function onLoadSubmit() {
            document.online_payment_form.submit();
        }
    </script>
</head>

<body onload="onLoadSubmit();">
<br/>&nbsp;<br/>
<center><font size="5" color="#3b4455">Transaction is being processed,<br/>Please wait ...</font></center>


<form action="<?php echo $payment_url; ?>" method="POST" name="online_payment_form">

    <?php do_action('wpi_payment_fields_wpi_' . self::PAYMENT_ALIAS, $invoice); ?>

    <?php //do_action('wpi_after_payment_fields', $invoice); ?>

    <noscript>
        <br/>&nbsp;<br/>
        <center>
            <font size="3" color="#3b4455">
                JavaScript is currently disabled or is not supported by your browser.<br/>
                Please click Submit to continue the processing of your transaction.<br/>&nbsp;<br/>
                <input type="submit"/>
            </font>
        </center>
    </noscript>
</form>
</body>
</html>
