<?php
$pgdomain = $wpi_settings['billing']['wpi_accosapg']['settings']['DomainUrl']['value'];
$payment_url = "https://$pgdomain/AccosaPG/verify.jsp";
?>
<form action="<?php echo $payment_url; ?>" method="POST" name="online_payment_form"
      id="online_payment_form-<?php print $this->type; ?>"
      class="wpi_checkout online_payment_form <?php print $this->type; ?> clearfix">
    <?php if (!is_recurring()): ?>

        <?php do_action('wpi_payment_fields_' . $this->type, $invoice); ?>


        <?php do_action('wpi_after_payment_fields', $invoice); ?>
        <?php $invoice_disabled = $invoice['net'] <= 0; ?>

        <div class="warning">
        <?php if ($invoice_disabled): ?>
            Amount can not be $0 or less.
        <?php else:?>
         &nbsp;&nbsp;&nbsp;&nbsp;
        <?php endif; ?>
        </div>

        <button type="submit" id="cc_pay_button" <?php echo $invoice_disabled ? 'disabled' : '' ?>
                class="btn btn-pay "><?php _e('Make payment', ud_get_wp_invoice()->domain); ?>

        </button>
        <img style="display: none;" class="loader-img"
             src="<?php echo ud_get_wp_invoice()->path("static/styles/images/processing-ajax.gif", 'url'); ?>"
             alt=""/>


    <?php else: ?>
        <p><?php _e('This payment gateway does not support Recurring Billing. Try another one or contact site Administrator.', ud_get_wp_invoice()->domain); ?></p>
    <?php endif; ?>

    <style>
        #wp_invoice_process_wait button:disabled {
            background-color: lightgrey;
            border-color: lightgrey;
            color: black;

        }

        div.warning {
            color: red;
            margin-bottom: 10px;
            font-size: 13px;
            font-weight: bold;
            margin-top: -26px;
        }
    </style>