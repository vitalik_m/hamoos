<?php
/**
 * Created by mary
 * Email: mary.ushenina@gmail.com
 * Date: 30.03.18
 * Time: 17:02
 */

/**
 * Name: ACCOSAPG
 * Class: wpi_accosapg
 * Internal Slug: wpi_accosapg
 * JS Slug: wpi_accosapg
 * Version: 1.0
 * Description: Provides the ACCOSAPG for payment options
 */
class wpi_accosapg extends wpi_gateway_base
{

    private static $pgdomain;
    private static $pgInstanceId;
    private static $merchantId;
    private static $hashKey;

    private static $response = null;

    const SUCCESS_PAYMENT_STATUS_CODE = '50020';
    const SUCCESS_TEST_PAYMENT_STATUS_CODE = '50097';
    const CANSELLED_PAYMENT_GATEWAY_CODE = '50011';

    const PAYMENT_ALIAS = 'accosapg';

    public function __construct()
    {

        parent::__construct();

        $this->options = array(
            'name' => 'ACCOSAPG',
            'allow' => '',
            'default_option' => '',
            'settings' => array(
                'DomainUrl' => array(
                    'label' => __("Domain Url for submit", ud_get_wp_invoice()->domain),
                    'value' => '',
                    'description' => __('IPG data submiting to this DOMAIN with URL https://{DOMAIN}/AccosaPG/verify.jsp', ud_get_wp_invoice()->domain),
                    'default' => __('paystage.com', ud_get_wp_invoice()->domain),
                ),

                'PGInstanceId' => array(
                    'label' => __("PGInstance Id", ud_get_wp_invoice()->domain),
                    'value' => '',
                    'description' => __('collection of intiger numbers, given by bank.', ud_get_wp_invoice()->domain),
                ),

                'MerchantID' => array(
                    'label' => __("Merchant ID", ud_get_wp_invoice()->domain),
                    'value' => '',
                    'description' => __('Unique ID for the merchant acc, given by bank.', ud_get_wp_invoice()->domain),
                ),

                'HashKey' => array(
                    'label' => __("Hash Key", ud_get_wp_invoice()->domain),
                    'value' => '',
                    'description' => __('Collection of mix intigers and strings , given by bank.', ud_get_wp_invoice()->domain),
                ),
                'ipn' => array(
                    'label' => __("Status URL", ud_get_wp_invoice()->domain),
                    'type' => "readonly",
                    'description' => __("Use this URL as Status URL in Merchant settings to get notified once payments made.", ud_get_wp_invoice()->domain)
                )

            )
        );

        $this->options['settings']['ipn']['value'] = admin_url('admin-ajax.php?action=wpi_gateway_server_callback&type=wpi_accosapg');

        global $invoice;

        $settings = $invoice['billing']['wpi_accosapg']['settings'];

        self::$pgdomain = $settings['DomainUrl']['value'];
        self::$pgInstanceId = $settings['PGInstanceId']['value'];
        self::$merchantId = $settings['MerchantID']['value'];
        self::$hashKey = $settings['HashKey']['value'];
    }

    /**
     * Fields renderer
     * @param type $invoice
     */
    function wpi_payment_fields($invoice)
    {
        $testFail = false;
//        $testFail = false;

//        <input type="radio" name="perform" value="initiatePaymentCapture#sale" checked/> Sale
//		  <input type="radio" name="perform" value="initiatePaymentCapture#preauth"/> Pre Auth

        $settings = $invoice['billing']['wpi_accosapg']['settings'];

        $pgInstanceId = $settings['PGInstanceId']['value'];
        $merchantId = $settings['MerchantID']['value'];
        $hashKey = $settings['HashKey']['value'];

        $perform = 'initiatePaymentCapture#sale';
//        $perform='initiatePaymentCapture#preauth';

        $currencyCode = $this->getCurrencyCode();
        $amount = $this->getMoneyAmount($invoice);
        $merchantReferenceNo = $invoice['ID'];;
        $orderDesc = $invoice['post_title'] . (!empty($invoice['post_content']) ? "<br/>" . $invoice['post_content'] : '');

        $messageHash = $pgInstanceId . "|" . $merchantId . "|" . $perform . "|" . $currencyCode . "|" . $amount . "|" . $merchantReferenceNo . "|" . $hashKey . "|";
        $message_hash = "CURRENCY:7:" . base64_encode(sha1($messageHash, true));

        ?>

        <input type="hidden" name="pg_instance_id" value="<?php echo $pgInstanceId; ?>"/>
        <input type="hidden" name="merchant_id" value="<?php echo $merchantId; ?>"/>

        <?php if ($testFail): ?>
        <!--        $testFail:true-->
        <input type="hidden" name="perform" value="initiatePaymentCapture#preauth"/>
    <?php else: ?>
        <input type="hidden" name="perform" value="<?php echo $perform; ?>"/>
    <?php endif; ?>

        <input type="hidden" name="currency_code" value="<?php echo $currencyCode; ?>"/>
        <input type="hidden" name="amount" value="<?php echo $amount; ?>"/>
        <input type="hidden" name="merchant_reference_no" value="<?php echo $merchantReferenceNo; ?>"/>
        <input type="hidden" name="order_desc" value="<?php echo $orderDesc; ?>"/>
        <input type="hidden" name="message_hash" value="<?php echo $message_hash; ?>"/>

        <?php
    }

    /**
     * Settings for recurring billing
     * @param type $this_invoice
     */
    function recurring_settings($invoice)
    {
        ?>
        <h4><?php _e('ACCOSAPG Recurring Billing', ud_get_wp_invoice()->domain); ?></h4>
        <p><?php _e('Currently InterKassa gateway does not support Recurring Billing', ud_get_wp_invoice()->domain); ?></p>
        <?php
    }

    private static function getHumanAmount($amount)
    {
        $amount = (int)$amount;
        $temp = $amount / 100;
        return $temp;
    }

    private function _hash_verified_response($invoice)
    {
        $settings = $invoice['billing']['wpi_accosapg']['settings'];

        $messageHash = $_POST["message_hash"];

        $hashData = array(
            $settings['PGInstanceId']['value'],
            $settings['MerchantID']['value']
        );
        $hashFields = [
            'transaction_type_code',
            'installments',
            'transaction_id',
            'amount',
            'exponent',
            'currency_code',
            'merchant_reference_no',
            'status',
            '3ds_eci',
            'pg_error_code',
        ];
        $othersFields = [
            'pg_error_detail',
            'pg_error_msg',
        ];
        foreach ($hashFields as $fieldName) {
            $hashData[] = self::$response[$fieldName] = $_POST[$fieldName];

        }
        foreach ($othersFields as $fieldName) {
            self::$response[$fieldName] = $_POST[$fieldName];
        }

        $hashData[] = $settings['HashKey']['value'];

//        $messageHashBuf=$pgInstanceId."|".$merchantId."|".$transactionTypeCode."|".$installments."|".$transactionId."|".$amount."|".$exponent."|".$currencyCode."|".$merchantReferenceNo."|".$status."|".$eci."|".$pgErrorCode."|".$hashKey."|";

        $messageHashBuf = implode('|', $hashData) . '|';

        $messageHashClient = "13:" . base64_encode(sha1($messageHashBuf, true));

        return ($messageHash == $messageHashClient);

    }

    /**
     * Merchant CB handler
     * Full callback URL: http://domain/wp-admin/admin-ajax.php?action=wpi_gateway_server_callback&type=wpi_accosapg
     */
    static function server_callback()
    {
        //var_dump($_POST);die();
        if (empty($_POST))
            die(__('Direct access not allowed', ud_get_wp_invoice()->domain));

        $invoice = new WPI_Invoice();
        $invoice->load_invoice("id={$_POST['merchant_reference_no']}");


        if (!self::_hash_verified_response($invoice->data)) {
            $message = ' Internal Server Error [Hash or Shop ID is wrong]';
            header($_SERVER['SERVER_PROTOCOL'] .' 500'. $message, true, 500);
            die(__($message, ud_get_wp_invoice()->domain));
            return;
        }
        //check if customer press "cancel" OR TIMEOUT!!
        if (self::$response['status'] == self::CANSELLED_PAYMENT_GATEWAY_CODE){
            $message = 'Your order cancelled';
            $link = '<a href="'.home_url().'">back</a>';
            die(__($message.' '.$link, ud_get_wp_invoice()->domain));
            return;
            //header('Location:'.home_url().''); // incorrect
        }

        else if (self::$response['status'] != self::SUCCESS_PAYMENT_STATUS_CODE &&
            self::$response['status'] != self::SUCCESS_TEST_PAYMENT_STATUS_CODE) {
            $message =  'Internal Server Error [Cannot process payment]. ' . $_POST["pg_error_detail"];
            header($_SERVER['SERVER_PROTOCOL'] .' 500 '. $message, true, 500);
            die(__($message, ud_get_wp_invoice()->domain));
        }

        // check wpi_processed_by_accosapg flag
        else if (get_post_meta($invoice->data['ID'], 'wpi_processed_by_accosapg', 1) == 'true') {
            $message = ' Internal Server Error [Already processed]';
            header($_SERVER['SERVER_PROTOCOL'].' 500 ' . $message, true, 500);
            die(__($message, ud_get_wp_invoice()->domain));
            return;
        }



        update_post_meta($invoice->data['ID'], 'wpi_processed_by_accosapg', 'true');

        /** Add payment amount */
        $humanAmount = self::getHumanAmount($_POST['amount']);

        $extranote = self::$response['status'] == self::SUCCESS_TEST_PAYMENT_STATUS_CODE ? ' (TEST transaction)' : '';
        $event_note = sprintf(__("%s paid via ACCOSAPG{$extranote}", ud_get_wp_invoice()->domain), WPI_Functions::currency_format(abs($humanAmount), $_POST['merchant_reference_no']));
        $event_amount = (float)$humanAmount;
        $event_type = 'add_payment';

        //** Log balance changes */
        $invoice->add_entry("attribute=balance&note=$event_note&amount=$event_amount&type=$event_type");

        //** Log payer email */
        $trans_id = sprintf(__("Transaction ID: %s", ud_get_wp_invoice()->domain), $_POST['transaction_id']);
        $invoice->add_entry("attribute=invoice&note=$trans_id&type=update");
        $invoice->save_invoice();

        //** ... and mark invoice as paid */
        wp_invoice_mark_as_paid($_POST['merchant_reference_no'], $check = true);

        send_notification($invoice->data);

        //if ok -> redirect to html-invoice page > the status is Paid
      
        echo '<script type="text/javascript">window.location="' . get_invoice_permalink($invoice->data['ID']) . '";</script>';
        echo 'OK';

    }

    /**
     * get the Currency code from ISO_4217 standart
     * @return mixed
     */
    private function getCurrencyCode()
    {
        global $wpi_settings;

        $currency_list = require_once(ud_get_wp_invoice()->path("lib/third-party/accosapg/ISO_4217.php", 'dir'));
        $global_currency = $wpi_settings['currency']['default_currency_code'];

        return $currency_list[$global_currency][1];
    }

    /**
     * get the price amount including decimals, meaning * 100
     * @param $invoice
     * @return float|int
     */
    private function getMoneyAmount($invoice)
    {
        $amount = floatval($invoice['net']);
//        $amount = number_format($amount, 2);

        return $amount * 100;
    }


    public static function getAutoSubmitPage($invoice_object)
    {
        global $wpi_settings;

        $invoice = $invoice_object->data;

        $pgdomain = $wpi_settings['billing']['wpi_accosapg']['settings']['DomainUrl']['value'];
        $payment_url = "https://$pgdomain/AccosaPG/verify.jsp";

        return require_once(ud_get_wp_invoice()->path("lib/gateways/templates/wpi_accosapg-autosubmit.php", 'dir'));

    }


    function frontend_display($args = '', $from_ajax = false) {
        global $wpdb, $wpi_settings, $invoice;
        //** Setup defaults, and extract the variables */
        $defaults = array();
        extract(wp_parse_args($args, $defaults), EXTR_SKIP);
        $process_payment_nonce = wp_create_nonce( "process-payment" );
        include(ud_get_wp_invoice()->path("lib/gateways/templates/{$this->type}-frontend.tpl.php", 'dir'));
    }

}