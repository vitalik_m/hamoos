<?php
/**
 * ACCOSAPG response code statuses
 */

return [
    '50010' => 'Init',
    '50011' => 'Capture Aborted',
    '50012' => '3DS Start',
    '50013' => '3DS Completed',
    '50014' => '3DS Failed',
    '50015' => '3DS Aborted',
    '50016' => 'Switch Start',
    '50017' => 'Switch Timeout',
    '50018' => 'Switch Aborted',
    '50020' => 'Success',
    '50021' => 'Failed',
    '50097' => 'Test Transaction',

];