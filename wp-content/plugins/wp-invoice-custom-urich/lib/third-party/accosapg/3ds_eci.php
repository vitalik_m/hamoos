<?php
/**
 * Created by PhpStorm.
 * User: dev-4
 * Date: 03.04.18
 * Time: 15:26
 */

return [
//ECI suggested by MP

//ECI for MasterCard
    '00' => 'failed authentication',
    '01' => 'authentication failed/not available',
    '02' => 'successful authentication',

//ECI for Visa
    '05'=> 'successful authentication',
    '06'=> 'attempted authentication',
    '07'=> 'failed authentication',

];