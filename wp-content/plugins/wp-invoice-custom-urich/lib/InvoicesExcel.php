<?php
    class InvoicesExcel
    {
        public $objPHPExcel;
        public $invoices;
        public $countRows;
        public $params = [
            'numberposts' => '-1',
            'post_type' => 'wpi_object',
            'post_status' => 'all',
            'orderby' => 'ID',
            'order' => 'DESC',
        ];

        const HEADER_ROW = 4;
        const FILTERS = [
            'wplt_filter_s' => 'Title',
            'wplt_filter_invoice_id' => 'ID',
            'wplt_filter_post_date_min' => 'Date from',
            'wplt_filter_post_date_max' => 'Date to',
            'wplt_filter_post_status' => 'Status',
            'wplt_filter_type' => 'Type',
            'wplt_filter_user_email' => 'Recipient'
        ];

        public function __construct()
        {
            require_once dirname(__FILE__) . '/classes/PHPExcel.php';
            require_once dirname(__FILE__) . '/classes/PHPExcel/Style/Fill.php';
            $this->objPHPExcel = new PHPExcel();

            $this->setFilters();

            $this->invoices = $this->getInvoices();

            $this->countRows = self::HEADER_ROW +1;
        }

        public function exportAll()
        {
            $this->createExcel();

            $this->setRowsStyles();

            $this->setFiltersRow();

            $this->setFooter();

// Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Hamoos Invoices.xls"');
            header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $this->objPHPExcel->getActiveSheet()->setTitle("Aggregate Report");

            $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }

        private function createExcel()
        {
            $this->setTitleRow();
            $this->setHeader();

            foreach($this->invoices as $index => $invoice){
                if(!$this->kastenValidation($invoice)) continue;

                $this->setInvoiceRow($invoice);
            }
        }

        private function getInvoices()
        {
            if(!empty($filters['post_title'])) $params['post_title'] = $filters['post_title'];
            if(!empty($filters['ID'])) $params['ID'] = $filters['ID'];

            return $invoices = get_posts($this->params);
        }

        public function setInvoiceRow($invoice)
        {
            $rowNum = $this->countRows;

            $invoiceMetas = get_post_meta( $invoice->ID );
            $user = unserialize($invoiceMetas['user_data'][0]);

            $this->objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$rowNum, $invoiceMetas['invoice_id'][0])
                ->setCellValue('B'.$rowNum, $invoice->post_date)
                ->setCellValue('C'.$rowNum, $user['display_name'] . ' ('. $user['user_nicename'] .')')
                ->setCellValue('D'.$rowNum, $invoiceMetas['user_email'][0])

                ->setCellValue('E'.$rowNum, $invoice->post_title)
                ->setCellValue('F'.$rowNum, $this->getOrderDetails($invoiceMetas['table_details'][0]))

                ->setCellValue('G'.$rowNum, $this->getDetails($invoiceMetas['itemized_list'][0], '0'))
                ->setCellValue('H'.$rowNum, $this->getDetails($invoiceMetas['itemized_list'][0], '1'))

                ->setCellValue('I'.$rowNum, 'View Invoice')
                ->setCellValue('J'.$rowNum, $invoice->post_status)
                ->setCellValue('K'.$rowNum, $this->getInvoicePrice($invoice, $invoiceMetas))
                ->setCellValue('L'.$rowNum, $invoiceMetas['type'][0]);

            $this->objPHPExcel->setActiveSheetIndex(0)
                ->getCell('I'.$rowNum)->getHyperlink()->setUrl($this->makeAdminLinkInvoice($invoiceMetas));

            $this->setRowStyle($rowNum);

            $this->countRows = $this->countRows + 1;
        }

        private function setTitleRow()
        {
            $styles = [
                'alignment' => [
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                ],
                'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => 'ffffff'],
                ]
            ];
            $this->objPHPExcel->getActiveSheet()->getStyle('A1:K3')->applyFromArray($styles);

            $this->objPHPExcel->getActiveSheet()
                ->mergeCells('A1:O1')
                ->setCellValue('A1', 'Aggregate Invoice Report')

                ->mergeCells('A2:O2')
//                ->setCellValue('A2', 'Filters: Date From 08/01/2019; Date To 08/31/2019; Status: Paid')

                ->mergeCells('A3:O3');

            $stylesTit1e = [
                'font'  => [
                    'bold'  => true,
                    'size'  => 14,
                ]
            ];
            $this->objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($stylesTit1e);
        }

        private function setHeader()
        {
            $this->objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.self::HEADER_ROW, 'ID')
                ->setCellValue('B'.self::HEADER_ROW, 'Date')
                ->setCellValue('C'.self::HEADER_ROW, 'Guest Name')
                ->setCellValue('D'.self::HEADER_ROW, 'Guest Email')

                ->setCellValue('E'.self::HEADER_ROW, 'Toure title')
                ->setCellValue('F'.self::HEADER_ROW, 'Toure Details ')

                ->setCellValue('G'.self::HEADER_ROW, 'Adult details')
                ->setCellValue('H'.self::HEADER_ROW, 'Child details')

                ->setCellValue('I'.self::HEADER_ROW, 'Invoice')
                ->setCellValue('J'.self::HEADER_ROW, 'Status')
                ->setCellValue('K'.self::HEADER_ROW, 'Amount')
                ->setCellValue('L'.self::HEADER_ROW, 'Type');

            $headerFont = [
                'font'  => [
                    'bold'  => true,
                    'color' => ['rgb' => 'FFFFFF'],
                ],
                'alignment' => [
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ],
                'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => '305496'],
                ],
            ];
            $this->objPHPExcel->getActiveSheet()->getStyle('A'.self::HEADER_ROW.':L'.self::HEADER_ROW)->applyFromArray($headerFont);

            $this->hideNotUsedCells(self::HEADER_ROW);
        }

        private function setFooter()
        {
            $styles = [
                'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => '123213'],
                ]
            ];

            $this->objPHPExcel->getActiveSheet()->getStyle('A'. $this->countRows+1 .':Q'.$this->countRows+3)
                ->applyFromArray($styles);
        }

        private function getOrderDetails($invoiceMetas)
        {
            $details = unserialize($invoiceMetas);

            $detailFields = '';

            foreach($details as $type => $detail){
                $detailFields .= $type .': '.$detail['value'] . '
';
            }

            return $detailFields;
        }

        private function getDetails($invoiceMetas, $key = null)
        {
            $details = unserialize($invoiceMetas);
            if(!is_array($details) || !is_array($details[$key])) return '';

            $details = is_array($details[$key]) ? $details[$key] : '
';

            $detailFields = '';

            foreach($details as $type => $detail){
                $detailFields .= $type .': '.$detail . ' ';
            }

            return $detailFields;
        }

        private function setRowsStyles(){
            $this->objPHPExcel->getActiveSheet()->getStyle('A2:L'.$this->countRows)
                ->getAlignment()->setWrapText(true);

            $this->objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(35);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
            $this->objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        }

        private function setRowStyle($rowNum)
        {
            $fillColor = $rowNum % 2 == 0 ? 'ddebf7' : 'FFFFFF';

            $styles = [
                'alignment' => [
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ],
                'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => $fillColor],
                ],
                'borders' => [
                    'allborders' => [
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    ]
                ]
            ];

            $stylesLink = [
                'font'  => [
                    'underline'  => true,
                    'color' => ['rgb' => '#0073aa'],
                ]
            ];

            $this->objPHPExcel->getActiveSheet()->getStyle('A'.$rowNum.':L'.$rowNum)->applyFromArray($styles);
            $this->objPHPExcel->getActiveSheet()->getStyle('I'.$rowNum)->applyFromArray($stylesLink);

            $this->hideNotUsedCells($rowNum);
        }

        private function hideNotUsedCells($rowNum)
        {
            $styles = [
                'fill' => [
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => ['rgb' => 'ffffff'],
                ]
            ];

            $this->objPHPExcel->getActiveSheet()->getStyle('M1:Q'.$rowNum)->applyFromArray($styles);
        }

        private function getInvoiceLinks($invoice, $invoiceMetas)
        {
            $adminLink = $this->makeAdminLinkInvoice($invoice);

            $showLink = site_url().'invoice/?invoice_id=' . $invoiceMetas->hash[0];

            return '=HYPERLINK("'. $adminLink .'","View Invoice")&CHAR(10)&HYPERLINK("' . $showLink . '","Show Invoice")';
        }

        private function makeAdminLinkInvoice($invoiceMetas)
        {
            return site_url().'/invoice/?invoice_id='.$invoiceMetas['hash'][0];
        }

        private function setFilters()
        {
            $params = array_intersect_key($_GET, self::FILTERS);
            if(!$params) return;

            foreach($params as $key => $value){
                call_user_func([$this, 'fillPostsParam_' . $key], $value);
            }
        }

        private function fillPostsParam_wplt_filter_s($title)
        {
            $this->params['post_title'] = $title;
        }

        private function fillPostsParam_wplt_filter_invoice_id($id)
        {
            $this->params['meta_query']['relation'] = 'AND';

            $arr = [
                'key' => 'invoice_id',
                'value' => $id
            ];

            $this->params['meta_query'][] = $arr;

            return $arr;
        }

        private function fillPostsParam_wplt_filter_post_date_min($time)
        {
            $arr = [
                'after'     => $time,
                'inclusive' => true,
            ];

            $this->params['date_query'][] = $arr;

            return $arr;
        }

        private function fillPostsParam_wplt_filter_post_date_max($time)
        {
            $arr = [
                'before'    => $time,
                'inclusive' => true,
            ];

            $this->params['date_query'][] = $arr;

            return $arr;
        }

        private function fillPostsParam_wplt_filter_post_status($status)
        {
            $this->params['post_status'] = $status;
        }

        private function fillPostsParam_wplt_filter_type($type)
        {
            $this->params['meta_query']['relation'] = 'AND';

            $arr = [
                'key' => 'type',
                'value' => $type
            ];

            $this->params['meta_query'][] = $arr;

            return $arr;
        }

        private function fillPostsParam_wplt_filter_user_email($email)
        {
            $this->params['meta_query']['relation'] = 'AND';

            $arr = [
                'key' => 'user_email',
                'value' => $email
            ];

            $this->params['meta_query'][] = $arr;

            return $arr;
        }

        private function setFiltersRow()
        {
            $filters = 'Filters: ';

            foreach (self::FILTERS as $key => $value){
                if(!empty($_GET[$key])){
                    $filters .= $this->makeFiltersRowPart($value, $_GET[$key]);
                }
            }

            $this->objPHPExcel->getActiveSheet()
                ->setCellValue('A2', $filters);
        }

        private function makeFiltersRowPart($filter, $value)
        {
            return $filter . ': '. $value . '; ';
        }

        private function getInvoicePrice($invoice, $invoiceMetas)
        {
            if($invoice->post_status == 'paid'){
                return '$'.$invoiceMetas['price'][0];
            }else{
                return $invoiceMetas['price'][0] ? '$0 of $'.$invoiceMetas['price'][0] : '';
            }
        }

        private function kastenValidation($invoice)
        {
            return $invoice->post_status == $this->params['post_status'] || $this->params['post_status'] == 'any';
        }

    }