<?php
/**
 * Created by mary
 * Email: mary.ushenina@gmail.com
 * Date: 30.03.2018
 * Time: 12:19
 *
 * This class allows to save Html invoice to PDf file
 */

if (!class_exists('WPI_PDF_Invoice')) {

//    error_reporting(2047);
//    ini_set('display_errors', 1);

    if (!class_exists('TCPDF')) {
        require_once(ud_get_wp_invoice()->path('lib/third-party/tcpdf/tcpdf.php', 'dir'));
    }

    class PDFG extends TCPDF
    {
        //PDF Page header
        public function Header()
        {
        }

        // PDF Page footer
        public function Footer()
        {
        }
    }


    class WPI_PDF_Invoice
    {
        private $contentHtml;

        private $fileName = 'hamoos_invoice_№ID.pdf';

        private $lang;

        const SAVE_ALIAS = 'save';
        const SEND_ALIAS = 'send';

        const POSTFIX_ALIAS = 'Pdf';

        const HTML_FOR_PDF = 'htmlpdf';
        const GET_PARAMETR = 'pdf';

        const ID_POST_TERMS = '6882';

        private $invoiceId;


        public static $availibleDoing = [
            self::SAVE_ALIAS . self::POSTFIX_ALIAS, // method savePdf
            self::SEND_ALIAS . self::POSTFIX_ALIAS, // method sendPdf
        ];

        /**
         * WPI_PDF_Invoice constructor.
         * @param WPI_Invoice $wpi
         */
        public function __construct(WPI_Invoice $wpi)
        {
            global $l;

            $this->invoiceId = $wpi->data['ID'];
            $this->contentHtml = $this->getHTML($this->invoiceId);



            $this->fileName = "hamoos_invoice_No{$this->invoiceId}_from_" . date('Y-m-d_H-i') . ".pdf";
            $this->lang = $l;
        }


        /**
         * get html prepared version for pdf
         * @param int $POST_Id
         * @return bool|string
         */
        private function getHTML($POST_Id)
        {
            $url = self::get_invoice_permalink_for_pdf($POST_Id, self::HTML_FOR_PDF);

            $content = file_get_contents($url);

            $content = $this->concatTerms($content);

            return $content;
        }

        /**
         * save pdf file
         */
        public function savePdf($saveToFolder = false)
        {
            $pdf = new PDFG(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Hamoos');
            $pdf->SetTitle('Hammos Pdf Invoice');
            $pdf->SetSubject('PDF Invoice');

            $pdf->SetMargins(15, 0, 15);
            $pdf->SetHeaderMargin(0);

            // convert TTF font to TCPDF format and store it on the fonts folder
            $path=ud_get_wp_invoice()->path("Muli-Regular.ttf", 'dir');
            $fontname = TCPDF_FONTS::addTTFfont($path, 'TrueTypeUnicode', '', '32'); //96

            $path=ud_get_wp_invoice()->path("Muli-Bold.ttf", 'dir');
            $fontnamebold = TCPDF_FONTS::addTTFfont($path, 'TrueTypeUnicode', '', '32'); //96

            // use the font
            $pdf->SetFont($fontname, '', 13, '', false);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once(dirname(__FILE__) . '/lang/eng.php');
                $pdf->setLanguageArray($this->lang);
            }

            // add a page
            $pdf->AddPage();

            ob_start();
            $pdfHtml = <<<EOD
$this->contentHtml
EOD;

            $pdf->writeHTML($pdfHtml, true, false, true, false, '');
            ob_end_clean();

            if ($saveToFolder) {

                $full_file_name = WP_CONTENT_DIR . '/uploads/' . $this->fileName;
                $pdf->Output($full_file_name, 'F');
                return $full_file_name;

            } else {

                $pdf->Output($this->fileName, 'D'); // I -view // D-save
                self::logEvent(['invoice_id' => $this->invoiceId], [], 'savePdf');
            }
        }

        public static function getLInkFile($absolute_file_path)
        {
            return str_replace(ABSPATH, WP_SITEURL, $absolute_file_path);
        }

        /**
         *  send pdf file to customer via email
         */
        public function sendPdf()
        {
            //save to temp directory
            $full_pdf_file = $this->savePdf(true);

            $invoice = ['invoice_id' => $this->invoiceId];
            //send email to customer with attachment
            send_notification($invoice, [$full_pdf_file]);

            self::logEvent($invoice, [], 'sendPdf');
        }

        /**
         * generate get_invoice_permalink with extra pdf parametes
         * @param $id
         * @param $value
         * @return string
         */
        public static function get_invoice_permalink_for_pdf($id, $value)
        {
            return get_invoice_permalink($id) . "&" . self::GET_PARAMETR . '=' . $value;
        }

        private static function logEvent($invoice, $extra_data, $type = 'default')
        {
            $pretty_time = date(get_option('time_format') . " " . get_option('date_format'), time() + get_option('gmt_offset') * 60 * 60);

            $text = self::getLogTextByType($type, $extra_data)
                . ' '. __('at', ud_get_wp_invoice()->domain)
                . " {$pretty_time}.";

            $action_type = ($type == 'savePdf') ? 'save_pdf' : 'notification';
            WPI_Functions::log_event(wpi_invoice_id_to_post_id($invoice['invoice_id']), 'invoice', $action_type, '', $text, time());
        }


        private static function getLogTextByType($type, $data = [])
        {
            switch ($type) {
                case 'savePdf':
                    return __("Generated and saved Pdf Invoice", ud_get_wp_invoice()->domain)
                        . (isset($data['user']) ? ' ' . __('by', ud_get_wp_invoice()->domain) . " {$data['user']}" : '');
                    break;

                case 'sendPdf':
                    return __("Notification Sent with Pdf Invoice", ud_get_wp_invoice()->domain)
                        . (isset($data['to']) ? " " . __('to', ud_get_wp_invoice()->domain) . " {$data['to']} " : '');
                    break;

                default:
                    return 'default log text';
                    break;
            }
        }

        private function concatTerms($content)
        {
            if(get_post(self::ID_POST_TERMS)){
                return $content.'<div style="
                    font-size: 8px;
                ">'. apply_filters('the_content', get_post(self::ID_POST_TERMS)->post_content) .'</div>';
            }

            return $content;
        }
    }

}