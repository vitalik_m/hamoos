<?php
/**
 * Created by mary
 * Email: mary.ushenina@gmail.com
 * Date: 28.03.2018
 * Time: 14:19
 *
 * This class allows to generate new Wp-invoice from the Contact-Form-7 post Data
 *
 * $FROM_DATA=[
your-name,
your-phone,
your-email,
your-ar-date,
your-dep-date,
your-nat,
your-adults,
your-child,
your-remarcs,
your-tour-url
    ];
 *
 */

if (!class_exists('WPI_CF7_Invoice')) {

    trait Tour
    {

        private $tour;

        /**
         * get Toug slug from url
         * @return mixed|string
         */
        private function getTourSlug()
        {
            if (isset($this->tour['slug'])) {
                return $this->tour['slug'];
            }
            $slug = 'undefined';
            if (isset($this->inputData[self::FORM_FIELDS_ALIAS['url']])) {
                $temp_url_name = str_replace(get_site_url(), '', $this->inputData[self::FORM_FIELDS_ALIAS['url']]);
                $tourName = trim($temp_url_name, '/');
                $temp = explode('/', $tourName);
                $slug = array_pop($temp);
            }
            $this->tour['slug'] = $slug;
            return $slug;
        }

        /**
         * get Tour object
         * @return array|false|WP_Term
         */
        private function getTour()
        {
            if (isset($this->tour['term'])) {
                return $this->tour['term'];
            }

            $slug = $this->getTourSlug();
            $term = get_term_by('slug', $slug, 'category');

            $this->tour['term'] = $term;
            return $term;
        }

        /**
         * get tour meta data
         * @return mixed|void
         */
        private function getTourMeta()//**----------------------SEE HERE-----------------------**//
        {
            if (isset($this->tour['meta'])) {
                return $this->tour['meta'];
            }

            $term = $this->getTour();
            if(!$term){
                return [];
            }
            $t_id = $term->term_id;
            $meta = get_option("category_$t_id");

            $this->tour['meta'] = $meta;
            return $meta;
        }

        /**
         * get tour ticket price if from categories
         * @return int
         */
        private function getTourTicketPriceCategories()//**----------------------SEE HERE-----------------------**//
        {
            $meta = (array) $this->getTourMeta();

            //sorry for this kasten :(
            if(!empty($this->inputData['_wpcf7']) && ($this->inputData['_wpcf7'] == 331 || $this->inputData['_wpcf7'] == 1499)){
                if(empty($meta['price'])){
                    $meta['price'] = $this->inputData['amount'];
                }
            }

            return isset($meta['price']) ? (float) $meta['price'] : 0;
        }

        /**
         * if from posts 'casinopack'
         * @return bool|int|mixed
         */
        private function getTourTicketPricePosts()
        {

            if (isset($this->inputData[self::FORM_FIELDS_ALIAS['url']])) {
                $url = $this->inputData[self::FORM_FIELDS_ALIAS['url']];

                $prePage = explode("?", substr($url, 1));
                $url = $prePage[0];

                $temp_url_name = str_replace(get_site_url(), '', $url);
                $tourName = trim($temp_url_name, '/');
                $temp = explode('/', $tourName);
                $slug = array_pop($temp);

                $args = array(
                    'name' => $slug,
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'numberposts' => 1
                );

                $my_posts = get_posts($args);
                if (count($my_posts) == 0) {
                    return false; //error post not found
                }

                $price_meta = get_post_meta($my_posts[0]->ID, 'price', true);
                return isset($price_meta) ? $price_meta : 0;
            }

            return false;
        }

        /**
         * get tour name
         * @return mixed|string
         */
        private function getTourName()
        {
            $term = $this->getTour();
            if ($term) {
                $tourName = $term->name;
            } else {
                $tourName = $this->getTourSlug();
            }

            return $tourName;
        }


    }

    class WPI_CF7_Invoice
    {

        use Tour;

        private static $taxRate = 0;

        const ADULTS='adults';
        const CHILDREN='child';

        const FORM_FIELDS_ALIAS = [
            'name' => 'your-name',
            'phone' => 'your-phone',
            'email' => 'your-email',
            'ar-date' => 'your-ar-date',
            'dep-date' => 'your-dep-date',
            'nationality' => 'your-nat',
            self::ADULTS => 'your-adults',
            self::CHILDREN => 'your-child',
            'package-name' => 'your-package',
            'remarcs' => 'your-remarcs',
            'url' => 'your-tour-url'
        ];


        /**
         * products list and fields settings
         * @var array
         */
        private static $Products = [
            self::ADULTS => [
                'name' => 'adult ticket',
                'description' => '',
            ],
            self::CHILDREN => [
                'name' => 'child ticket',
                'description' => '',
            ],

        ];

        /**
         * the input data that was passed in Class
         * @var array
         */
        private $inputData = [];

        /**
         * generated Data for invoice
         * @var array
         */
        private $data = [
            'created_by' => 'CategoryFormInvoiceGenerator',
            'type' => 'invoice',
        ];


        private $errors = [];

        /**
         * WPI_CF7_Invoice constructor.
         * @param array $inputData
         */
        public function __construct($inputData = [])
        {
            $this->inputData = $inputData;
            $this->data['subject'] = $this->getInvoiceSubject();
//            $this->data['description'] = $this->getInvoiceDesctiption();
            $this->data['user_data'] = $this->getUserData();
            $this->generateExtraDetailsData();
            $this->generateItemizedList();
//var_dump($this->data);
        }

        /**
         * if data not exisst get from tour url and name
         * @return mixed|string
         */
        public function getInvoiceSubject()
        {
            if (isset($this->inputData[self::FORM_FIELDS_ALIAS['package-name']]) && !empty($this->inputData[self::FORM_FIELDS_ALIAS['package-name']])) {
                return $this->inputData[self::FORM_FIELDS_ALIAS['package-name']];
            }
            return $this->getTourName();
        }

        /**
         * convert input user data to invoice uder data
         * @return array
         */
        private function getUserData()
        {
            return [
                'user_email' => $this->inputData[self::FORM_FIELDS_ALIAS['email']],
                'first_name' => $this->inputData[self::FORM_FIELDS_ALIAS['name']],
                'last_name' => '',
                'phonenumber' => $this->inputData[self::FORM_FIELDS_ALIAS['phone']]
            ];
        }

        /**
         * get specific user data field
         * @param $field
         * @return string
         */
        public function getUserDataField($field)
        {
            if (isset($this->data['user_data'][$field])) {
                return $this->data['user_data'][$field];
            } else {
                return "user $field not found";
            }
        }

        /**
         * get tax setting
         * @return int
         */
        private function getTaxRate()
        {
            return self::$taxRate;
        }

        /**
         * calculate and combine all price and tax fields for invoice
         * @param $price
         * @param $count
         * @return array
         */
        private function calculatePriceAndTaxesFields($price, $count)//**----------------------SEE HERE-----------------------**//
        {
            //var_dump('4 - calculatePriceAndTaxesFields:');var_dump($price);
            $tax_rate = $this->getTaxRate();
            return [
                'quantity' => $count,
                'price' => $price, // todo: может цена у детского билета другая? узнать?
                'tax_rate' => $tax_rate,
                'line_total_tax' => $price * $tax_rate,
                'line_total_before_tax' => $price * $count,
                'line_total_after_tax' => ($price * $count) * $tax_rate,
            ];
        }

        /**
         * get the fieldnames list for products
         * @return array
         */
        private function getItemizedFiledList()
        {
            return array_keys(self::$Products);
        }

        /**
         * generate all item data/products for invoice
         */
        private function generateItemizedList($customCheck=null)
        {
            $this->data['itemized_list'] = [];

            $fields = $this->getItemizedFiledList();
            foreach ($fields as $fieldName) {
                if (!empty($this->inputData[self::FORM_FIELDS_ALIAS[$fieldName]]) || $customCheck == true) {
                    $this->addProductItem($fieldName);
                }
            }
        }

        /**
         * add item/product data to list
         * @param $fieldName
         */
        private function addProductItem($fieldName=null)//**----------------------SEE HERE-----------------------**//
        {
            $pricePerTicket = $this->getTourTicketPriceCategories();
//            if ($pricePerTicket == 0) {
////                $pricePerTicket = $this->getTourTicketPricePosts();
////            }
            $extraData = $this->calculatePriceAndTaxesFields($pricePerTicket, $this->inputData[self::FORM_FIELDS_ALIAS[$fieldName]]);
            $this->data['itemized_list'][] = array_merge([
                'name' => self::$Products[$fieldName]['name'],
                'description' => self::$Products[$fieldName]['description'],
            ], $extraData);
        }

        /**
         * get the invoice text field/description data string/template
         * @return string
         */
        private function getInvoiceDesctiption()
        {
            //todo: do the normal template
            ob_start();
            echo "Arriving date ={$this->inputData[self::FORM_FIELDS_ALIAS['ar-date']]}<br/>
Departure date={$this->inputData[self::FORM_FIELDS_ALIAS['dep-date']]}<br/>
Nationality={$this->inputData[self::FORM_FIELDS_ALIAS['nationality']]}<br/>"
                . (!empty($this->inputData[self::FORM_FIELDS_ALIAS['remarcs']]) ? "Remarks={$this->inputData[self::FORM_FIELDS_ALIAS['remarcs']]}<br/>" : '');
            $invoice_description = ob_get_contents();
            ob_end_clean();

            return $invoice_description;
        }

        /**
         * generate 'table_details' from extra form fields
         */
        private function generateExtraDetailsData()
        {
            $this->data['table_details'] = [
                'Arriving date' => ['type' => 'text', 'value' => $this->inputData[self::FORM_FIELDS_ALIAS['ar-date']]],
                'Departure date' => ['type' => 'text', 'value' => $this->inputData[self::FORM_FIELDS_ALIAS['dep-date']]],
                'Nationality' => ['type' => 'text', 'value' => $this->inputData[self::FORM_FIELDS_ALIAS['nationality']]],
            ];
            if (!empty($this->inputData[self::FORM_FIELDS_ALIAS['package-name']])) {
                $this->data['table_details']['Casino Package'] = ['type' => 'text', 'value' => $this->inputData[self::FORM_FIELDS_ALIAS['package-name']]];
            }
            if (!empty($this->inputData[self::FORM_FIELDS_ALIAS['remarcs']])) {
                $this->data['table_details']['Remarks'] = ['type' => 'textarea', 'value' => $this->inputData[self::FORM_FIELDS_ALIAS['remarcs']]];
            }

            $this->data['table_details']['Tour URL'] = ['type' => 'url', 'value' => $this->inputData[self::FORM_FIELDS_ALIAS['url']]];
        }
        /**
         * ggenerate new WPI_Invoice on cf7 from data
         * @return bool
         */
        public function generate($autoSubmit=false)
        {
            if (!class_exists('WPI_Invoice')) {
                $this->errors[] = 'Class WPI_Invoice not found';
                return false;
            }

            $pricePerTicket = $this->getTourTicketPriceCategories();
            if ($pricePerTicket == 0) {
                $pricePerTicket = $this->getTourTicketPricePosts();
            }
            $this->data['price'] = $pricePerTicket;

            $invoice = new WPI_Invoice();
            $invoice->set($this->data);
            $invoice->create_new_invoice();
            $email = isset($this->inputData[self::FORM_FIELDS_ALIAS['email']]) ? $this->inputData[self::FORM_FIELDS_ALIAS['email']] : 'noreply@hamoos.com';
            $invoice->load_user("email=$email");

            if (empty($invoice->data['user_data'])) {
                $user_id = register_new_user($email, $email);
                update_user_meta($user_id, 'first_name', $this->getUserDataField('first_name'));
                update_user_meta($user_id, 'phonenumber', $this->getUserDataField('phonenumber'));
                $invoice->load_user("user_id=$user_id");
            }


            if ($createdId = $invoice->save_invoice()) {
                if ($autoSubmit) {

                    if (!function_exists('wpi_payment_gateway_autosubmit_link')) {
                        require_once(ud_get_wp_invoice()->path('lib/class_template_functions.php', 'dir'));
                    }
                    return wpi_payment_gateway_autosubmit_link($invoice->data);

                } else {
                    return $createdId;
                }
            } else {
                return false; // or  make error
            }

        }

        /**
         * get last error
         * @return array|mixed
         */
        public function getLastError()
        {
            if (count($this->errors) > 0)
                return end($this->errors);
            else
                return [];
        }

        /**
         * get all errors
         * @return array
         */
        public function getErrors()
        {
            return $this->errors;
        }

        /**
         * preview the generated invoice data
         * @return array
         */
        public function getGeneratedData()
        {
            return $this->data;
        }
    }

}