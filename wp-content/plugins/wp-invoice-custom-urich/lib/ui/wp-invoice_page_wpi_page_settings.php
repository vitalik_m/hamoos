<?php
global $wpi_settings;

require_once __DIR__.'/wpi-settings-page.php';

$wpi_settings_tabs = array(
    'basic' => array(
        'label' => __('Main', ud_get_wp_invoice()->domain),
        'position' => 10,
        'callback' => array('WPI_Settings_page', 'basic')
    ),
    'business_process' => array(
        'label' => __('Business Process', ud_get_wp_invoice()->domain),
        'position' => 20,
        'callback' => array('WPI_Settings_page', 'business_process')
    ),
    'payment' => array(
        'label' => __('Payment', ud_get_wp_invoice()->domain),
        'position' => 30,
        'callback' => array('WPI_Settings_page', 'payment')
    ),
    'email_templates' => array(
        'label' => __('E-Mail Templates', ud_get_wp_invoice()->domain),
        'position' => 50,
        'callback' => array('WPI_Settings_page', 'email_templates')
    ),
    'predefined' => array(
        'label' => __('Line Items', ud_get_wp_invoice()->domain),
        'position' => 60,
        'callback' => array('WPI_Settings_page', 'predefined')
    ),
    'help' => array(
        'label' => __('Help', ud_get_wp_invoice()->domain),
        'position' => 500,
        'callback' => array('WPI_Settings_page', 'help')
    ),
    'direct'=>array(
        'label' => __('Direct Bank Requisites', ud_get_wp_invoice()->domain)
            . " <span style=\"font-size: 10px;color:red;font-weight:bold;\">" . __('New', ud_get_wp_invoice()->domain) . "</span>",
        'position' => 600,
        'callback' => array('WPI_Settings_page', 'direct_bank_requisites')
    ),
);

// Allow third-party plugins and premium features to insert and remove tabs via API
$wpi_settings_tabs = apply_filters('wpi_settings_tabs', $wpi_settings_tabs);

//** Put the tabs into position */
usort($wpi_settings_tabs, create_function('$a,$b', ' return $a["position"] - $b["position"]; '));

if (isset($_REQUEST['message'])) {
  switch ($_REQUEST['message']) {
    case 'updated':
      WPI_Functions::add_message(__("Settings updated.", ud_get_wp_invoice()->domain));
      break;
  }
}
?>
<script type="text/javascript">

  var wpi = {
    'currency':'<?php echo $wpi_settings['currency']['symbol'][$wpi_settings['currency']['default_currency_code']]; ?> ',
    'thousandsSeparator':'<?php echo!isset($wpi_settings['thousands_separator_symbol']) ? ',' : ($wpi_settings['thousands_separator_symbol'] == '0' ? '' : $wpi_settings['thousands_separator_symbol']) ?>',
    'decimalSeparator':'<?php echo !isset( $wpi_settings['decimal_separator_symbol'] )?'.':($wpi_settings['decimal_separator_symbol'] == '0'?'':$wpi_settings['decimal_separator_symbol']) ?>'
  };

  jQuery(document).ready( function() {
    var wp_invoice_settings_page = jQuery("#wp_invoice_settings_page").tabs({cookie: {expires: 30, name: 'wp_invoice_settings_page_tabs'}});
    // The following runs specific functions when a given tab is loaded
    jQuery('#wp_invoice_settings_page').bind('tabsshow', function(event, ui) {
      var selected = wp_invoice_settings_page.tabs('option', 'selected');

      if(selected == 5) { }
    });
    // @TODO: Simple hack to fix setting page scrolling down on load. But cause of it not found.
    jQuery(this).scrollTop(0);
  });

</script>

<div class="wrap">
  <form method="post" id="wpi_settings_form" enctype="multipart/form-data">
    <?php echo WPI_UI::input("type=hidden&name=wpi_settings_update&value=true") ?>
    <h2><?php _e("WP-Invoice Global Settings", ud_get_wp_invoice()->domain) ?></h2>

    <?php WPI_Functions::print_messages(); ?>

    <div id="wp_invoice_settings_page" class="wpi_tabs wp_invoice_tabbed_content">
      <ul class="wp_invoice_settings_tabs tabs">
        <?php foreach ($wpi_settings_tabs as $tab_id => $tab) {
          if (!is_callable($tab['callback'])) continue; ?>
          <li><a href="#wpi_tab_<?php echo $tab_id; ?>"><?php echo $tab['label']; ?></a></li>
        <?php } ?>
      </ul>

      <?php foreach ($wpi_settings_tabs as $tab_id => $tab) { ?>
        <div id="wpi_tab_<?php echo $tab_id; ?>" class="wp_invoice_tab" >
          <?php
          if (is_callable($tab['callback'])) {
            call_user_func($tab['callback'], $wpi_settings);
          } else {
            echo __('Warning:', ud_get_wp_invoice()->domain) . ' ' . implode(':', $tab['callback']) . ' ' . __('not found', ud_get_wp_invoice()->domain) . '.';
          }
          ?>
        </div>
      <?php } ?>

    </div><?php /* end: #wp_invoice_settings_page */ ?>
    <div id="poststuff" class="metabox-holder">
      <div id="submitdiv" class="postbox" style="">
        <div class="inside">
          <div id="major-publishing-actions">
            <div id="publishing-action">
              <input type="submit" value="<?php esc_attr(_e('Save All Settings', ud_get_wp_invoice()->domain)) ?>" class="button-primary">
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div><?php /* end: .wrap */ ?>

