<style>
  .ud-badge {
    background: url( "<?php echo ud_get_wp_invoice()->path( '/static/images/icon.png', 'url' ); ?>" ) no-repeat center !important;
    background-size: 150px 150px !important;
    box-shadow: none !important;
  }
</style>
<div class="changelog">

  <div class="overview">

    <h2><?php printf( __( '%s %s has been installed', ud_get_wp_invoice()->domain ), ud_get_wp_invoice()->name, ud_get_wp_invoice()->args['version'] ); ?></h2>

    <p><?php printf( __( 'Congratulations! You have just installed brand new version of %s plugin. There are some important things that you need to be aware of in order to use our products successfully and with pleasure.', ud_get_wp_invoice()->domain ), ud_get_wp_invoice()->name) ?></p>

    <p><?php _e( 'Please read the following instructions carefully. Explore links below to get more information on our site.', ud_get_wp_invoice()->domain ); ?></p>

    <hr />

    <p><i><?php printf( __( '%s lets WordPress blog owners send itemized invoices to their clients. Ideal for web developers, SEO consultants, general contractors, or anyone with a WordPress blog and clients to bill.', ud_get_wp_invoice()->domain ), ud_get_wp_invoice()->name); ?></i></p>

  </div>

</div>