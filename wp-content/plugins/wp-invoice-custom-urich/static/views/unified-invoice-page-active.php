<?php
/**
 * Unified Invoice Page template
 *
 * Displays Single invoice page
 */
global $invoice, $wpi_settings;
$background_url=get_template_directory_uri().'/img/invoice-web-bg.png';
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) & !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
  <meta charset="<?php bloginfo('charset'); ?>"/>
  <meta name="viewport" content="width=device-width"/>
  <title><?php
    // Print the <title> tag based on what is being viewed.
    global $page, $paged;

    wp_title('|', true, 'right');

    // Add the blog name.
    bloginfo('name');

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && (is_home() || is_front_page()))
      echo " | $site_description";

    ?></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
  <?php wp_head(); ?>
  <!--[if lt IE 9]>
  <script src="http://cdnjs.com/libraries/html5shiv"></script>
  <script src="https://cdnjs.com/libraries/respond.js"></script>
  <![endif]-->

  <script type="text/javascript">
    jQuery(document).ready(function(){
      jQuery('body').unified_page_template();
    });
  </script>

  <link rel="stylesheet" href="/wp-content/plugins/wp-invoice-custom-urich/static/views/styles.css" />

</head>

<body class="inner-pages" >
<div class="background-invoice" style="background:url('<?php echo $background_url;?>'); width: 100%;height: 100%;background-repeat: no-repeat;
        background-size: cover;">

  <header class="pageheader">

    <div class="container">

      <div class="row header-info">

        <?php if ( show_business_info() ) : ?>

        <div class="col-sm-4">
          <?php if ( $logo_url = wpi_get_business_logo_url() ): ?>
            <div class="logo"><img style="max-width: 90px;" src="<?php echo $logo_url; ?>" alt="Logo" /></div>
          <?php endif; ?>
            <?php if ( $business_name = wpi_get_business_name() ): ?>
              <h1><?php echo $business_name; ?></h1>
            <?php endif; ?>
            <?php if ( $business_address = wpi_get_business_address() ): ?>
              <p><?php echo $business_address; ?></p>
            <?php endif; ?>
        </div>

        <div class="col-sm-5 contacts">
          <div class="contact">
            <?php if ( $business_email = wpi_get_business_email() ): ?>
            <p><span class="ico mail"></span>
              <a href="mailto:<?php echo $business_email; ?>"><?php echo $business_email; ?></a></p>
            <?php endif; ?>
            <?php if ( $business_phone = wpi_get_business_phone() ): ?>
              <p><span class="ico tel"></span> <?php echo $business_phone; ?></p>
            <?php endif; ?>
          </div>
        </div>
        <?php else: ?>
          <div class="col-m-12" style="height: 100px;"></div>
        <?php endif; ?>

      </div>

      <div class="row top-nav-links">

        <div class="col-xs-3" >
          <?php if ( wpi_dashboard_is_active() ): ?>
            <a href="<?php echo wpi_get_dashboard_permalink( $invoice['ID'] ); ?>" class="btn btn-back"> <?php _e( 'My dashboard', ud_get_wp_invoice()->domain ); ?></a>
          <?php endif; ?>
<!--          <a href="--><?php //echo get_invoice_tour_url(['return'=>true]); ?><!--" class="btn btn-pay"> --><?php //_e( 'Back to website', ud_get_wp_invoice()->domain ); ?><!--</a>-->
        </div>
        <div class="col-xs-6" >
<!--            <span class="text-right" style="font-weight:bold; font-size:  23px; line-height: 45px; display: inline-block; width: 100%; text-align: center;">Payment Progress</span>-->
            <?php /*if ($logo_url = wpi_get_business_logo_url()): ?>
                <img class="pull-right" style="display:float:right; " src="<?php echo $logo_url; ?>" alt="Logo"/>
            <?php endif;*/ ?>
        </div>
        <div class="col-xs-3 text-right" >
                  <!-- ********************************************************** -->
        </div>
      </div>

    </div><!--end /container-->

  </header><!--end /pageheader-->

  <div class="page-content" id="invoice-page-content">

    <div class="container" id="invoice-data-container">

      <div class="box-content">
          <div class="col-xs-12 text-center head-payment-invoice" >
              Payment Progress
          </div>

        <div class="box-inner-content">
            <?php include __DIR__.'/parts/_header_and_user_info.php'; ?>

          <div class="invoice-desc">
              <?php include __DIR__.'/parts/_extra_description.php'; ?>
          </div>

            <?php include __DIR__.'/parts/_items_table.php'; ?>


            <br/>
            <?php include __DIR__.'/parts/_after_info.php'; ?>

            <?php  $method = !empty($invoice['default_payment_method']) ? $invoice['default_payment_method'] : 'manual';?>
            <div class="row top-nav-links">
                <div class="col-sx-3 text-right bottom-payment"> <?php if($method=='wpi_accosapg' && is_invoice()):
                    if (!empty($wpi_settings['installed_gateways'][$method])) {
                    echo '   <img src="'. wp_get_upload_dir()['baseurl'].'/2018/06/visa-mc-paystage.png">';
                        $wpi_settings['installed_gateways'][$method]['object']->frontend_display($invoice);
                    } else {
                        _e('Sorry, there is no payment method available. Please contact Administrator.', ud_get_wp_invoice()->domain);
                    }

                elseif ( is_invoice() ): ?>
                    <a href="javascript:void(0);" id="close-payment-form" class="btn btn-pay"><?php _e('Go Back', ud_get_wp_invoice()->domain); ?></a>
                    <a href="javascript:void(0);" id="open-payment-form" class="btn btn-pay"><?php _e('Make Payment', ud_get_wp_invoice()->domain); ?></a>
                <?php endif; ?>

                <?php do_action('wpi_unified_template_top_navigation'); ?>
                </div>
            </div>
        </div><!--end /box-inner-content-->

      </div>

      <?php ob_start(); comments_template(); ob_clean(); ?>

      <?php if ( ( is_quote() || is_invoice() ) && have_comments() ): ?>
      <div id="quote-responses">
        <h4><?php _e('Discussion Thread', ud_get_wp_invoice()->domain); ?></h4>

        <div id="comments" class="box-content">

          <div class="box-inner-content">
            <ul>
              <?php wp_list_comments(); ?>
            </ul>

            <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
              <nav id="comment-nav-above" class="comment-navigation">
                <div class="nav-previous"><?php previous_comments_link( __( '&larr; Previous Page', ud_get_wp_invoice()->domain ) ); ?></div>
                <div class="nav-next"><?php next_comments_link( __( 'Next Page &rarr;', ud_get_wp_invoice()->domain ) ); ?></div>
                <div class="clearfix"></div>
              </nav><!-- #comment-nav-above -->
              <div class="clearfix"></div>
            <?php endif; // Check for comment navigation. ?>
          </div>

          <div class="clearfix"></div>

        </div>
      </div>
      <?php endif; ?>

      <?php if ( is_quote() ): ?>
      <div id="quote-response-form">
        <h4><?php _e('Leave a Response', ud_get_wp_invoice()->domain); ?></h4>
        <div class="box-content">
          <div class="box-inner-content">
            <?php comment_form(); ?>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <?php endif; ?>

      <?php do_action('wpi_unified_template_before_actions_history'); ?>

      <?php if ( $history = wpi_get_invoice_log(array(
          'refund' => __('Refund', ud_get_wp_invoice()->domain),
//          'notification' => __('Email', ud_get_wp_invoice()->domain),
//          'save_pdf' => __('Save Pdf', ud_get_wp_invoice()->domain),
//          'add_charge' => __('Charge', ud_get_wp_invoice()->domain),
          'add_payment' => __('Paid', ud_get_wp_invoice()->domain),
//          'do_adjustment' => __('Adjustment', ud_get_wp_invoice()->domain),
//          'create' => __('Create', ud_get_wp_invoice()->domain)
      )) ): ?>
      <div class="invoice-history">

        <h4><?php _e('Invoice History', ud_get_wp_invoice()->domain); ?></h4>

        <div class="box-content">
          <div class="box-inner-content">
            <?php foreach( $history as $hitem ): ?>
            <div class="row">
              <div class="col-md-2 label-item"><span class="label label-<?php echo $hitem['action']; ?>"><?php echo $hitem['label']; ?></span></div>
              <div class="col-md-7 description"><?php echo $hitem['text']; ?></div>
              <div class="col-md-3 date"><?php echo $hitem['time']; ?></div>
            </div>
            <?php endforeach; ?>
          </div><!--end /box-inner-content-->
        </div>
      </div>
      <?php endif; ?>

    </div><!--end /container-->

    <?php if (!is_quote()) : ?>
    <div id="payment-form-container" class="container invoice-payment">

      <div class="box-content">

        <div class="box-inner">

          <div class="wpi_checkout">
            <?php if (allow_partial_payments()): ?>

              <?php show_partial_payments(); ?>
            <?php endif; ?>

            <?php show_payment_selection(); ?>

            <?php
            $method = !empty($invoice['default_payment_method']) ? $invoice['default_payment_method'] : 'manual';
            if ($method == 'manual') {
              ?>
              <p><strong><?php _e('Manual Payment Information', ud_get_wp_invoice()->domain); ?></strong></p>
              <p><?php echo !empty($wpi_settings['manual_payment_info']) ? $wpi_settings['manual_payment_info'] : __('Contact site Administrator for payment information please.', ud_get_wp_invoice()->domain); ?></p>
            <?php
            } else {
              if (!empty($wpi_settings['installed_gateways'][$method])) {
                $wpi_settings['installed_gateways'][$method]['object']->frontend_display($invoice);
              } else {
                _e('Sorry, there is no payment method available. Please contact Administrator.', ud_get_wp_invoice()->domain);
              }
            }
            apply_filters("wpi_closed_comments", $invoice);
            ?>
          </div>

        </div><!--end /box-inner-content-->
      </div>

    </div><!--end /container-->
    <?php endif; ?>

  </div><!--end /page-content-->

  <div id="invoice-payment-success" class="page-content thankyou">

    <div class="container">

      <div class="box-content">

        <div class="box-inner-content">

          <div class="payment-logo">
            <img src="<?php echo ud_get_wp_invoice()->path('static/img/payment.png', 'url'); ?>" alt="" />
          </div>

          <h2><?php _e('Payment Sent Successfully', ud_get_wp_invoice()->domain); ?></h2>

          <p><?php _e('Thank you for your payment. You can check the invoice at your Invoices Dashboard.', ud_get_wp_invoice()->domain); ?></p>

          <div class="success-buttons">
            <a href="<?php echo get_invoice_permalink( $invoice['ID'] ); ?>" class="btn btn-success"><?php _e( 'Check Receipt', ud_get_wp_invoice()->domain ); ?></a>
            <?php if ( wpi_dashboard_is_active() ): ?>
              <a href="<?php echo wpi_get_dashboard_permalink( $invoice['ID'] ); ?>" class="btn btn-info"><?php _e( 'View Dashboard', ud_get_wp_invoice()->domain ); ?></a>
            <?php endif; ?>
          </div>

        </div><!--end /box-inner-content-->
      </div>

    </div><!--end /container-->

  </div><!--end /page-content-->

    <div class="container invoice-terms">
        <div class="box-content ">
            <?php include __DIR__.'/parts/_terms.php'; ?>
        </div>
    </div>


  <footer class="pagefooter">
    <div class="container">
        <a href="<?php echo get_invoice_tour_url(['return'=>true]); ?>" id="back-to-website-button"> < <?php _e( 'Back to website', ud_get_wp_invoice()->domain ); ?></a>

        <!--      <a href="https://www.usabilitydynamics.com/product/wp-invoice" target="_blank">-->
<!--        <p>Powered by <span><img src="--><?php //echo ud_get_wp_invoice()->path( 'static/img/wp-invoice.png', 'url' ); ?><!--" alt="WP-Invoice" /></span> WP-Invoice</p>-->
<!--      </a>-->
    </div><!--end /container-->
  </footer><!--end /pagefooter-->

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</div>
</body>

<?php wp_footer(); ?>

</html>