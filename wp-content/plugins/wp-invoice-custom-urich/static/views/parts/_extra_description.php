<strong>Details of the tour or tour number:</strong><br/>

<table class="extra_table_description" border="0" style="width: 100%" cellpadding="5" cellspacing="0">
    <tr>
        <td colspan="3" style="text-align: center; border: 1px solid lightgrey "><strong><?php echo wpi_get_invoice_title(); ?></strong></td>
    </tr>

    <?php if ($details_table = the_description_extra_table(['return' => true])): ?>
        <?php foreach ($details_table as $desc_key => $details): ?>
            <?php if (!empty($details['value']) && $details['type'] != 'url' && $desc_key != $details['value']): ?>
                <tr style="border: none">
                    <th style="border: 1px solid lightgrey; font-weight: bold; font-size:10px;"> <?php echo $desc_key ?> </th>
                    <td  colspan="2" style="border: 1px solid lightgrey; font-size:10px;"><?php echo $details['value'] ?></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if ($desc = the_description(['return' => true])): ?>
        <tr style="border: none">
            <th style="border: 1px solid lightgrey"> Description </th>
            <td style="border: 1px solid lightgrey" colspan="2"><?php the_description(); ?></td>
        </tr>
    <?php endif; ?>

</table>