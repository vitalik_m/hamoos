<?php
$invoice['hash'] ? $invoice_hash = $invoice['hash'] : $invoice_hash = $_GET['invoice_id'];
$download_link = get_home_url().'/invoice/?invoice_id='.$invoice_hash.'&pdf=save'; ?>
<?php $border = 0; ?>
<?php $header = wpi_get_invoice_type(true); ?>
<table border="<?php echo $border ?>" width="100%" cellspacing="0" ; cellpadding="0">

    <tr>
        <td style="vertical-align: middle"><span
                    style=" text-align: left; vertical-align: middle ;font-weight: bold; font-size: 25px; margin: 0">
                <?php _e($header, ud_get_wp_invoice()->domain); ?>
            </span>

        </td>
        <td style="text-align:right">
            <?php if ($logo_url = wpi_get_business_logo_url()): ?>
                <img style="display:float:right; " src="<?php echo $logo_url; ?>" alt="Logo"/>
            <?php endif; ?>
        </td>
    </tr>
    <tr style="text-align: left; margin: 0;">
        <td style="text-align: left;">
            <?php if($header=='Receipt') _e('your payment has been processed successfully',ud_get_wp_invoice()->domain);?>
        </td>
    </tr>
    <tr><td></td></tr>
    <tr><td><a class="btn btn-pay btn-info" href="<?php echo $download_link ?>">download</a> </td></tr>
    <tr>
        <td colspan="2" style="line-height: 1.2"><strong><?php _e('Invoice Number:', ud_get_wp_invoice()->domain); ?></strong>
            <?php invoice_id(); ?>
            <br/>

            <strong><?php _e('Date:', ud_get_wp_invoice()->domain); ?></strong>
            <?php echo wpi_get_invoice_issue_date(); ?>
            <br/>

            <?php if (wpi_invoice_has_due_date()): ?>
                <strong><?php _e('Due Date:', ud_get_wp_invoice()->domain); ?></strong>
                <?php echo wpi_get_invoice_due_date(); ?>
                <br/>
            <?php endif; ?>

            <strong><?php _e('Guest Name:', ud_get_wp_invoice()->domain); ?></strong>
            <?php recipients_name(); ?>.
            <?php echo wpi_get_company_address(); ?>
            <br/>

            <?php if ($recipients_email = recipients_email(array('return' => true))): ?>
                <strong><?php _e('Guest Email:', ud_get_wp_invoice()->domain); ?></strong>
                <a style="color: black" href="mailto:<?php echo $recipients_email; ?>"><?php echo $recipients_email; ?></a>
            <?php endif; ?>

            <?php do_action('wpi_unified_template_after_recipient'); ?>

        </td>
    </tr>
</table>
<br/>
