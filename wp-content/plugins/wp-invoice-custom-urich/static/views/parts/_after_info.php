
<?php if ($direct_bank_requisites = wpi_get_direct_bank_requisites()): ?>
    <strong><?php _e('Direct Bank Requisites:', ud_get_wp_invoice()->domain); ?></strong><br/>
    <?php echo $direct_bank_requisites; ?><br/>
<?php endif; ?>

<?php if ( $business_email = wpi_get_business_email() ): ?>
    <strong><?php _e('Email:', ud_get_wp_invoice()->domain); ?></strong> <a style="color: black" href="mailto:<?php echo $business_email; ?>"><?php echo $business_email; ?></a><br/>
<?php endif; ?>
<?php if ( $business_phone = wpi_get_business_phone() ): ?>
    <strong><?php _e('Phone:', ud_get_wp_invoice()->domain); ?></strong> <?php echo $business_phone; ?><br/>
<?php endif; ?>

<?php if ( show_business_info() ) : ?>
    <?php if ( $business_name = wpi_get_business_name() ): ?>
        <h1><?php echo $business_name; ?></h1>
    <?php endif; ?>
    <?php if ( $business_address = wpi_get_business_address() ): ?>
        <p><?php echo $business_address; ?></p>
    <?php endif; ?>
<?php endif; ?>



