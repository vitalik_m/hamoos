<?php if (wpi_invoice_has_items()): $colCount = 4; ?>
    <strong>Order details:</strong><br/>

    <?php $table_header_style = "font-weight:bold; text-align:center;  border: 1px solid lightgray;" ?>
    <table border="0" style="width: 100%" cellpadding="3" class="items_table">
        <tr>
            <th colspan="2" class="items-description"
                style="<?php echo $table_header_style ?>"><?php _e('Description', ud_get_wp_invoice()->domain); ?></th>
            <?php if (wpi_show_quantity_column()): $colCount++; ?>
                <th style="<?php echo $table_header_style ?>"><?php _e('Quantity', ud_get_wp_invoice()->domain); ?></th>
            <?php endif; ?>
            <th style="<?php echo $table_header_style ?>"><?php _e('Package cost', ud_get_wp_invoice()->domain); ?></th>
            <th style="<?php echo $table_header_style ?>"><?php _e('Amount', ud_get_wp_invoice()->domain); ?></th>
            <?php if (wpi_get_invoice_total_tax()): $colCount++ ?>
                <th style="<?php echo $table_header_style ?>"><?php _e('Tax', ud_get_wp_invoice()->domain); ?></th>
            <?php endif; ?>
        </tr>


        <?php $i = 0; $extrastyle=' border: 1px solid lightgray;font-size:10px; ';
        while ($line_item = wpi_get_line_item($i)) : ?>
            <tr>
                <td colspan="2" style="<?php echo $extrastyle?>">
                    <?php echo $line_item->get_name(); ?>
                    <?php if ($_description = $line_item->get_description()): ?>
                        <?php echo $_description; ?>
                    <?php endif; ?>
                </td>
                <?php if (wpi_show_quantity_column()): ?>
                    <td  style="text-align:center; <?php echo $extrastyle?>"><?php echo $line_item->get_quantity(); ?></td>
                <?php endif; ?>
                <td  style="text-align:center; <?php echo $extrastyle?>"><?php echo $line_item->get_price(wpi_get_invoice_currency_sign()); ?></td>
                <td  style="text-align:center; <?php echo $extrastyle?>"><?php echo $line_item->get_amount(wpi_get_invoice_currency_sign()); ?></td>
                <?php if (wpi_get_invoice_total_tax()): ?>
                    <td   style="text-align:center; <?php echo $extrastyle?>"><?php echo $line_item->get_tax(wpi_get_invoice_currency_sign()); ?></td>
                <?php endif; ?>
            </tr>
        <?php endwhile; ?>


        <?php if (wpi_invoice_has_charges()): ?>
            <tr>
                <td colspan="<?php echo $colCount ?>" style="<?php echo $extrastyle?>">
                    <table border="1" style="width: 100%" cellpadding="3">
                        <tr>
                            <td colspan="2">
                                <h4><?php _e('Additional Charges', ud_get_wp_invoice()->domain); ?></h4>
                            </td>
                        </tr>
                        <tr>
                            <th class="description"><?php _e('Description', ud_get_wp_invoice()->domain); ?></th>
                            <th class="amount"><?php _e('Amount', ud_get_wp_invoice()->domain); ?></th>
                        </tr>

                        <?php $i = 0;
                        while ($line_item = wpi_get_line_charge($i)) : ?>
                            <tr>
                                <td><?php echo $line_item->get_name(); ?></td>
                                <td><?php echo $line_item->get_amount(wpi_get_invoice_currency_sign()); ?></td>
                            </tr>
                        <?php endwhile; ?>

                    </table>
                </td>
            </tr>
        <?php endif; ?>

        <tr>
            <td colspan="<?php echo $colCount ?>" style="<?php echo $extrastyle?>">
                <?php $colspan = 3;
                $table_header_style = "font-weight:bold; width:80%; " ?>
                <table border="<?php echo $border ?>" style="width: 100%" cellpadding="3"
                       cellspacing="3">
                    <?php if (wpi_get_invoice_total_tax()): ?>
                        <tr class="total-row">
                            <td colspan="<?php echo $colspan ?>"
                                style="text-align:right; <?php echo $table_header_style ?>">
                                <span><?php _e('Total:', ud_get_wp_invoice()->domain); ?></span>
                            </td>
                            <td><?php echo wpi_get_total(wpi_get_invoice_currency_sign()); ?></td>
                        </tr>
                        <tr class="total-row">
                            <td colspan="<?php echo $colspan ?>"
                                style="text-align:right; <?php echo $table_header_style ?>">
                                <span><?php _e('Total Tax:', ud_get_wp_invoice()->domain); ?></span>
                            </td>
                            <td><?php echo wpi_get_invoice_total_tax(wpi_get_invoice_currency_sign()); ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if (wpi_get_discount()): ?>
                        <tr class="total-row">
                            <td colspan="<?php echo $colspan ?>"
                                style="text-align:right; <?php echo $table_header_style ?>">
                                <span><?php _e('Discount:', ud_get_wp_invoice()->domain); ?></span>
                            </td>
                            <td> <?php echo wpi_get_discount(wpi_get_invoice_currency_sign()); ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if (wpi_get_adjustments()): ?>
                        <tr class="total-row">
                            <td colspan="<?php echo $colspan ?>"
                                style="text-align:right; <?php echo $table_header_style ?>">
                                <span><?php _e('Other Adjustments:', ud_get_wp_invoice()->domain); ?></span>
                            </td>
                            <td><?php echo wpi_get_adjustments(wpi_get_invoice_currency_sign()); ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if (wpi_get_total_payments()): ?>
                        <tr class="total-row">
                            <td colspan="<?php echo $colspan ?>"
                                style="text-align:right;<?php echo $table_header_style ?>">
                                <span><?php _e('Amount:', ud_get_wp_invoice()->domain); ?></span>
                            </td>
                            <td><?php echo wpi_get_total_payments(wpi_get_invoice_currency_sign()); ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if($invoice['net']>0 || $invoice['post_status']=='active'): ?>
                    <tr class="total-row">
                        <td colspan="<?php echo $colspan ?>"
                            style="text-align:right; <?php echo $table_header_style ?>">
                            <span><?php _e('Amount:', ud_get_wp_invoice()->domain); ?></span>
                        </td>

                        <td><?php echo wpi_get_amount_due(wpi_get_invoice_currency_sign()); ?></td>
                    </tr>
                    <?php endif; ?>
                </table>
            </td>
        </tr>

    </table>
<?php endif; ?>