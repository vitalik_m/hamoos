<?php
/**
 * Unified Invoice Page template
 *
 * Displays Single invoice page
 */
global $invoice, $wpi_settings;
//var_dump($invoice);
//var_dump(home_url().'/invoice?invoice_id='.$_GET['invoice_id'].'&pdf=safe');die;
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) & !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width"/>
    <title><?php
        // Print the <title> tag based on what is being viewed.
        global $page, $paged;

        wp_title('|', true, 'right');

        // Add the blog name.
        bloginfo('name');

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page()))
            echo " | $site_description";

        ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <?php wp_head(); ?>

</head>


<body class="inner-pages">
            <?php $border = 0; ?>
            <div class="box-inner-content"><!--
                --><div><?php include __DIR__.'/parts/_header_and_user_info.php'; ?></div>
                <?php include __DIR__.'/parts/_extra_description.php'; ?>
            <br/>
            <br/>

                <?php include __DIR__.'/parts/_items_table.php'; ?>
                <?php if($header!='Receipt'): //check status for custom view pdf?>
                <?php if ($payment_link = wpi_payment_gateway_autosubmit_link()): ?>
                    <table>
                        <tr><td></td></tr>
                    </table>

                    <table>
                        <tr>
                            <td><a href="<?php echo $payment_link ?>" style="display;block">Pay Online</a></td>
                            <td><img src="<?php echo wp_get_upload_dir()['baseurl'];?>/2018/06/visa-mc-paystage.png" style="height: 20px;"></td>
                            <td></td><td></td><td></td>
                        </tr>
                    </table>
                    <table>
                        <tr><td></td></tr>
                    </table>



                <?php endif; ?>


                <?php include __DIR__.'/parts/_after_info.php'; ?>
                <?php else:?>
                    <table>
                        <tr><td></td></tr>
                        <tr><td style=" text-align: center;"> <?php _e('for more details', ud_get_wp_invoice()->domain); ?></td></tr>
                    </table>
                    <table border="<?php echo $border ?>" width="100%" cellspacing="0" ; cellpadding="0">

                        <tr>
                            <td style="vertical-align: middle"><span
                                        style="text-align: center; font-weight: bold; font-size: 18px; margin: 0">
                     <?php _e('Contact us', ud_get_wp_invoice()->domain); ?>
                        </span>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2" style="line-height: 1.2; text-align: center;"><strong><?php _e('Address : ', ud_get_wp_invoice()->domain); ?></strong>
                                  No 34, D.R. Wijewardena Mawatha,
                                  <br/>

                                  <strong><?php _e('Colombo 10, Sri Lanka', ud_get_wp_invoice()->domain); ?></strong>

                                  <br/>

                                  <strong><?php _e('24/7 Hot line : ', ud_get_wp_invoice()->domain); ?></strong>
                                + 94 77 8693205 +94 11 2334284
                                  <br/>


                                <strong><?php _e('E-mail:', ud_get_wp_invoice()->domain); ?></strong>
                                info@hamoos.com travels@hamoos.com
                                <br/>


                            </td>
                        </tr>
                    </table>


                <?php endif?>
            </div><!--end /box-inner-content-->




<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>


</html>