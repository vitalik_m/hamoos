jQuery('document').ready(function(){
    // jQuery('#posts_filter input').on( "change", function(){updateExcelLink()});
    // jQuery('#posts_filter select').on( "change", function(){updateExcelLink()});

    jQuery('#invoice_excel_import a').click(function(e){getInvoicesExcel(e) })

});

function updateExcelLink() {
    if(!window.invoicesExcelLink){
        window.invoicesExcelLink = jQuery('#invoice_excel_import a').attr('href');
    }

    jQuery('#invoice_excel_import a').attr('href', createNewInvoiceExcelLink());
}

function createNewInvoiceExcelLink() {
    let filters = '';

    jQuery('#posts_filter input').each((index, elem) => {
        elem = jQuery(elem);

        if(elem.val()){
            filters = filters + '&' + elem.attr('name') + '=' + elem.val();
        }
    });

    jQuery('#posts_filter select').each((index, elem) => {
        elem = jQuery(elem);

        if(elem.val()){
            filters = filters + '&' + elem.attr('name') + '=' + elem.val();
        }
    });

    return window.invoicesExcelLink+filters;
}

function getInvoicesExcel(e) {
    e.preventDefault();

    updateExcelLink();

    document.location.href = jQuery('#invoice_excel_import a').attr('href');
}