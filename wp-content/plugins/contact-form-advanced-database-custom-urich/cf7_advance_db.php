<?php

/**
 * Plugin Name: Contact Form Advance Database(custom Urich)
 * Description: Plugin is based on Contact Form Advance Database plugin(version 1.0.8) and customised with a few extra functions. A Very Simple plugin that will capture all the emails being sent using Contact Form 7 Plugin
 * Version: 1.0
 * Author: Urich
 * Author URI: http://urich.org/
 */
 define('CF7ADBURL',plugin_dir_url(__FILE__));
 require( 'lib/cf7_adb.class.php' );

 //initiate Main Class
 $CF7AdvanceDB = new CF7AdvanceDB();

 
