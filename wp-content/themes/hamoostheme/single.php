<?php
/*
Template Name: Casinos template
*/
get_header();

function getPriceMeta(){
    $post = get_post( );
    $price_meta = get_post_meta($post->ID, 'price', true);

    return isset($price_meta) ? $price_meta : 0;
}
?>

<style>
    .form-wrap .row {
        padding: 0 !important;
    }
</style>
<main>
    <?php if ( has_tag('packages')) {?>

    <div class="container-fluid">
<div class="for-go-back">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>/casinos" class="go-back">Go Back</a>
			</div>
        <div class="container pg-container inside-pg-container" id="pg-container">
            <div class="row">
                <div class="container thumbnail">
                    <div class="col-md-4">
                        <?php if ( has_post_thumbnail()) { ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                <?php the_post_thumbnail(); ?>
                            </a>
                        <?php } ?><div class="t2-img-arrow"></div>
                    </div>
                    <div class="col-md-8">
                        <h1 class="col-xs-12" id="casino-package">
                            <?php the_title();?>
                        </h1>
                        <?php if (($price = getPriceMeta()) > 0): ?>
			
                            <div class="col-xs-12 col-md-3 t2-wrap-item-highlighter price-field" id="casino-price">$ <?php echo $price ?></div>
                            <div class="col-xs-12 col-md-3 t2-wrap-item-highlighter buy-btn" onclick="buyFunc()"><span>Buy</span></div>
                            <div class="payment-cards">
                               <img class="payment-card" src="<?php echo get_template_directory_uri();?>/img/mastercard.png">
                               <img class="payment-card" src="<?php echo get_template_directory_uri();?>/img/visa.png">
                               <img class="payment-card" src="<?php echo get_template_directory_uri();?>/img/paystage.png">
                            </div>
			
                        <?php endif; ?>

                        <div class="col-xs-12 body-content">
                            <?php the_post();the_content();?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <article>
                    <div class="col-xs-12 pg-container-inner">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">

                            <h4 class="casino-icons-contacts col-xs-12 col-md-6">
                                <img src="http://hamoos.com/wp-content/themes/hamoostheme/img/phone.svg" class="icons">
                                <img src="http://hamoos.com/wp-content/themes/hamoostheme/img/viber.svg" class="icons">
                                <img src="http://hamoos.com/wp-content/themes/hamoostheme/img/whatsapp.svg"
                                     class="icons">
                                <b>English </b>(Telles) +94 774772042
                            </h4>
                            <h4 class="casino-icons-contacts col-xs-12 col-md-6">
                                <img src="http://hamoos.com/wp-content/themes/hamoostheme/img/phone.svg" class="icons">
                                <img src="http://hamoos.com/wp-content/themes/hamoostheme/img/viber.svg" class="icons">
                                <img src="http://hamoos.com/wp-content/themes/hamoostheme/img/whatsapp.svg"
                                     class="icons">
                                <b>हिंद </b>(Melan) +94 774776564
                            </h4>

                        </div>
                    </div>
                </article>
            </div>

            <div class="clearfix"></div>

            <div class="row dark-back-popup">
                <article>
                    <div class="col-sm-offset-3 col-md-6 col-sm-6 col-xs-12 pg-container-inner popup-tour">
                        <h1 class="col-xs-12 text-center">BUY NOW</h1>

                        <div class="col-xs-12 body-content">
<!--                            <p>For further information please send us your message by completing the form below.</p>-->
                            <div class="form-wrap">
                                <div class="row">
                                    <?php echo do_shortcode('[contact-form-7 id="217" title="Casinos"]'); ?>
                                </div>
                            </div>
                            <p> * marked fields are mandatory </p>
                        </div>
                    </div>

                </article>

            </div> <!-- END :: row -->
        </div> <!-- END :: container -->
    </div>
    <?php } ?>
    <?php if ( has_tag('attraction') ) { ?>
    <div class="container-fluid">
        <div class="container pg-container inside-pg-container" id="pg-container">
            <div class="row">
                <div class="container thumbnail">
                    <div class="col-md-4">
                        <?php if ( has_post_thumbnail()) { ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                <?php the_post_thumbnail(); ?>
                            </a>
                        <?php } ?><div class="t2-img-arrow"></div>
                    </div>
                    <div class="col-md-8">
                        <h1 class="col-xs-12">
                            <?php the_title();?>
                        </h1>
                        <div class="col-xs-12 body-content">
                            <?php the_post();the_content();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <div class="clearfix"></div>
    <?php } ?>
<?php if (  has_tag('promotions')  ) { ?>
    <div class="container-fluid">
        <div class="container pg-container inside-pg-container" id="pg-container">
            <div class="row">
                <div class="container thumbnail">
                    <div class="col-md-4">
                        <?php if ( has_post_thumbnail()) { ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                <?php the_post_thumbnail(); ?>
                            </a>
                        <?php } ?><div class="t2-img-arrow"></div>
                    </div>
                    <div class="col-md-8">
                        <h1 class="col-xs-12">
                            <?php the_title();?>
                        </h1>
                        <div class="col-xs-12 body-content">
                            <?php the_post();the_content();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php echo do_shortcode( '[contact-form-7 id="358" title="Promotions"]' ); ?>
            <div class="clearfix"></div>
    <?php } ?>
<?php if (  has_tag('thingstodo')  ) { ?>
    <div class="container-fluid">
        <div class="container pg-container inside-pg-container" id="pg-container">
            <div class="row">
                <div class="container thumbnail">
                    <div class="col-md-4">
                        <?php if ( has_post_thumbnail()) { ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                <?php the_post_thumbnail(); ?>
                            </a>
                        <?php } ?><div class="t2-img-arrow"></div>
                    </div>
                    <div class="col-md-8">
                        <h1 class="col-xs-12">
                            <?php the_title();?>
                        </h1>
                        <div class="col-xs-12 body-content">
                            <?php the_post();the_content();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php echo do_shortcode( '[contact-form-7 id="358" title="Promotions"]' ); ?>
            <div class="clearfix"></div>
    <?php } ?>
</main>
<script type="text/javascript">
//    var name = <?php //echo $getadults; ?>//;
    $( document ).ready(function() {
        $("#casinospack").val('<?php echo ucfirst(strtolower($_GET['casino_package'])); ?>');
        $('.buy-btn').click(function () {
            $('#casinoprice').closest('.price-form').find('label ').html('<br><br>Amount: '+$("#casinoprice").val()+'<br>');
        });
    });
</script>

<?php  get_footer()?>
