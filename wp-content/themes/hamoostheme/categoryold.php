<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<main>
	<div class="container-fluid">
		<div class="col-xs-12 t2-wrap">
			<div class="row">
<?php
$currentcategor = get_query_var('cat');
$test = get_query_var('category_name');

$posts = get_posts ($args1);
 $categors= get_the_category($post->ID);
//first get the current category ID
$cat_id = get_query_var('cat');
//then i get the data from the database
$cat_data = get_option("category_$cat_id");
//and then i just display my category image if it exists
$cat_cur_id=$categors[0]->category_parent;
$cat_cur_name=get_cat_name($cat_cur_id);

echo '<h1 class="text-uppercase col-xs-12 text-center dddd">'.$test.'</h1>';
if($currentcategor!=$categors[0]->category_parent ) {
    if (!empty($cat_data['dir'])) {
	echo '<div class="for-go-back">
		<a href="' . esc_url( home_url( '/' ) ). 'category/' . get_cat_slug($cat_cur_id) .'" class="go-back">Go Back</a>
	      </div>';
        echo '<div id="map"></div>';
    }
	
    if (!empty($cat_data['dn'])){
        echo '<div class="col-xs-12 col-md-3 t2-wrap-item-highlighter">'.$cat_data['dn'].'</div>';
    }
    foreach ($posts as $post) {
        foreach ($categors as $category) {
            if ($currentcategor != $category->category_parent) {
                echo '<div class="col-xs-12 itinerary-item">
                    <div class="col-xs-12 col-md-9 itinerary-desc-wrap">
                        <div class="col-md-1 col-sm-2 col-xs-12 itinerary-day">' . $post->post_title . '</div>
                        <div class="col-md-11  col-sm-10 col-xs-12 itinerary-title">' . $post->post_excerpt . '</div>
                        <div class="col-md-6 col-xs-12 itinerary-features">' . $post->post_content . '</div>                        
                    </div>
                     <div class="col-md-3 col-xs-12 itinerary-item-img">';
                the_post_thumbnail();
//                echo '<img src="' . get_stylesheet_directory_uri() . '/resources/newfiles/new-img/sigiriya-400.jpg" alt=""></div></div>';
                echo "</div></div>";
            }
        }
    }
    echo '</div></div>';   
    echo '<div class="row">
                <article>
                    <div class="col-xs-12 pg-container-inner">
                        <h1 class="col-xs-12 text-center">INQUIRY</h1>				
                        <div class="col-xs-12 body-content">
                            <p>For further information please send us your message by completing the form below.</p>
                            <div class="form-wrap">
                                <div class="row">
					' . do_shortcode( '[contact-form-7 id="331" title="Category Form"]' ) . '                                    
                                </div>
                            </div>
                            <p> * marked fields are mandatory </p>
                        </div>
                        </div>
                </article>
            </div>';
}
else{
 echo '<div class="for-go-back">
		<a href="' . esc_url( home_url( '/' ) ). 'tours/" class="go-back">Go Back</a>
	      </div>';
}

//echo $numcat;
$args = array(
	'orderby' => 'name',
	'child_of' => $currentcategor,
    'order' => 'DESC',
	'hide_empty' => '0'
);
$categories = get_categories($args);
foreach($categories as $category1) {
    if($category1->category_parent != 0) {
	    	
        echo '<div class="col-sm-6 col-xs-12 t2-wrap-item">';
        echo '<div class="t2-wrap-item-inner-wrap">';
        echo '<div class="col-xs-6 t2-wrap-item-img parent" >';
        echo do_shortcode(sprintf('[wp_custom_image_category term_id="%s"]',$category1->term_id));
//        echo '<img class="child" src="' .$siteurl.'/wp-content/uploads/' . $category1->slug . '-cats.jpg" alt="' . $category1->cat_name . '" />';
        echo '<div class="t2-img-arrow"></div></div>';
        echo '<div class="col-xs-6 t2-wrap-item-rgt">';
        echo '<div class="col-xs-12 t2-wrap-item-title">' .  $category1->cat_name . '</div>';
        echo '<div class="col-xs-12">
                      <div class="col-xs-12 t2-wrap-item-highlighter highlighter-wild-tours">Price on request</div></div>
                      <div class="col-xs-12 t2-wrap-item-desc">' . $category1->description . ' </div>
                      <div class="col-xs-12 t2-wrap-item-link" ><a title="View '.$category1->cat_name . ' in Sri Lanka"  href="' . get_category_link( $category1->term_id ) . '" class="find-out-more">find out more</a></div> </div>';
        echo '</div></div>';
    }
}
//$args1 = array(
//   'category' => $currentcategor,
///    'numberposts' => '10',
//    'orderby' => 'title',
//    'order' => 'asc'
//);
 //$posts = get_posts ($args1);
// $categors= get_the_category($post->ID);
//first get the current category ID
//$cat_id = get_query_var('cat');
//then i get the data from the database
//$cat_data = get_option("category_$cat_id");
//and then i just display my category image if it exists
//$cat_cur_id=$categors[0]->category_parent;
//$cat_cur_name=get_cat_name($cat_cur_id);



    ?>
	</div>
</main>
<script type="text/javascript">
    var cities = '<?php echo $cat_data['dir']; ?>',
        citiesArray = cities.split(",");
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBargFqa4N30PKodBTxL8xVSMgFUc3UVVM&callback=initMap">
</script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/js/map.js"></script>
<?php get_footer();?>

