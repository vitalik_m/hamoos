// $("#sriLanca").on("mouseover", function() {
//     $("#sriLancaTours").show();
//     $(".img-item").animate({left:"-3px"},500);
// });
// $("#sriLancaTours").on("mouseleave", function() {
//     $("#sriLancaTours").hide();
//     $(".img-item").animate({left:"-7px"},0);
// });
$(".highlights-cat-item").on("mouseover", function() {
    $(this).find(".highlights-cat-item-det").css({width:"100%", cursor:"pointer"});
    $(this).find(".highlights-cat-item-desc").css("opacity","1");
    $(this).find(".findout").css("opacity","1");
});
$(".highlights-cat-item").on("mouseleave", function() {
    $(".highlights-cat-item-det").css({width:"0px", cursor:"default"});
    $(".highlights-cat-item-desc").css("opacity","0");
    $(".findout").css("opacity","0");
});

$('.gal-cat-item a').on('click', function () {
    $(".gal-cat-item a").removeClass("selected");
    $(this).addClass("selected");
    $(".gal-item-group").fadeOut();
    $(".gal-group-" + $(this).data('id')).fadeIn();
    $(".gal-cat-mobile-drop").text($(this).text());
    console.log("111");

});

$(".gal-cat-mobile-drop").on("click", function () {
    $(".gal-cat-mobile-drop").toggleClass("fold");
    $(".gal-cat-mobile-drop").toggleClass("expand");
    $(".gal-cat-wrap").toggle();
    console.log("111");

});
$('#pop-up-close, .pop-up-content').on('click', function () {
    $('.back-shadow').fadeOut('slow');

});

$('.hotel-inquire').on('click', function () {
    var hotel = $(this).attr('title');
    $('#hotel').val(hotel);
});
