
// var cities = "Chicago,Indianapolis,Dayton,Columbus";
//  var   citiesArray = cities.split(",");

citiPoints = function () {
    var waypoints=[];

    for(var i = 1; i <citiesArray.length - 1; i++){
        waypoints.push(
            {
                location: citiesArray[i],
                stopover: true
            }
        )
    }
    return waypoints;
};

function initMap() {


    var map = new google.maps.Map(document.getElementById('map'), {
        center: citiesArray[citiesArray.length],
        scrollwheel: false,
        zoom: 7
    });

    var directionsDisplay = new google.maps.DirectionsRenderer({
        map: map
    });

    // Set destination, origin and travel mode.
    var request = {
        destination: citiesArray[citiesArray.length - 1],
        origin: citiesArray[0],
        waypoints: citiPoints(),
        travelMode: 'DRIVING'
    };

    // Pass the directions request to the directions service.
    var directionsService = new google.maps.DirectionsService();
    directionsService.route(request, function(response, status) {
        if (status == 'OK') {
            // Display the route on the map.
            directionsDisplay.setDirections(response);
        }
    });
}


