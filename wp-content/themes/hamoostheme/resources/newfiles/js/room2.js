var strroom = '<div class="prow prowdynamic clearfix">' +
    '<div class="item adultsNumber">' +
    '<div class="incrementinputholder clearfix inputholder">' +
    ' <label>Adults (12+yrs)</label>' +
    '<span class="minus button">-</span>' +
    '<input class="incrementinput hotel-adult" type="text" value="0">' +
    '<span class="plus button">+</span>' +
    '</div>' +
    '</div>' +

    '<div class="item children">' +
    '<div class="incrementinputholder clearfix inputholder">' +
    '<label>Children (0-12)</label>' +
    ' <span class="minus button">-</span>' +
    '<input class="incrementinput hotel-child" type="text" value="0">' +
    '<span class="plus button">+</span>' +
    '</div>' +
    '</div>' +

    '<div class="item typeOfRoom">' +
        '<div class="incrementinputholder">'+
        '<label>Type Of Room</label>' +
        '<select class="typeOfRoomSelect">' +
            '<option value="strandart">Standart</option>'+
            '<option value="lux">Lux</option>'
        '</select>' +
        '</div>' +
    '</div>' +

    '<div class="item childageholder">' +
    '</div>' +
    '</div>';

$(document).ready(function(){
    // Show count after click ok Travelers field on travelers popup

    $('.people, .trav-ok-btn').click(function () {
        $('#hotel-peoplewrap').slideToggle();
        $('#hotel-peoplewrap-form').slideToggle();
    })

    $(".paneldata .people").click(function(event) {
        var adlt = 0;
        var chld = 0;
        $(this).next().find('.prowwrap .prow .item .inputholder input.hotel-adult').each(function() {
            var type = $(this).attr("type");
            if ((type == "text")) {
                adlt += parseInt($(this).val());
                chld += parseInt($(this).parent().parent().next().find(".hotel-child").val());
            }
        });
        $(this).text( adlt + " Adults / " +  chld + " Children " );
    });

    // adding room row
    $('.addaroom').click(function(e) {
        e.preventDefault(), $(strroom).appendTo($(this).siblings('.prowwrap'));
    });
    // remove room row
    $('.removeroom').click(function(e) {
        e.preventDefault(), $(this).siblings('.prowwrap').find('.prowdynamic:last-of-type').remove();
    });

    $(document).on('click', '.button', function(event) {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;

        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
            if ($button.parents('.children').length) {
                $($button.parents('.children').next()).find('.incrementinputholder:nth-child(' + oldValue + ')').remove();
            }
        }

        $button.parent().find("input").attr('value', newVal);
    });
    function PeoplesToJson(){
        var adultChildArr = [];

        $('.prow').each(function()
        {
            self = $(this);

            adultChildArr.push(
                {
                    adult: $(this).find(".hotel-adult").val(),
                    child: $(this).find(".hotel-child").val(),
                    type: $(this).find('.typeOfRoomSelect').val()
                });
        });

        $('.guests').val(JSON.stringify(adultChildArr));
    }

    $(".trav-ok-btn").click(function(event) {
        var adlt = 0;
        var chld = 0;
        $(this).parent().find('.prowwrap .prow .item .inputholder input.hotel-adult').each(function() {
            var type = $(this).attr("type");
            if ((type == "text")) {
                adlt += parseInt($(this).val());
                chld += parseInt($(this).parent().parent().next().find(".hotel-child").val());
            }
        });
        $(this).parent().parent().find(".people").text( adlt + " Adults / " +  chld + " Children " );
        PeoplesToJson()
    });
})

