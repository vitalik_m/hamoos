var strroom = '<div class="prow prowdynamic clearfix">' +
    '<div class="item adultsNumber">' +
    '<div class="incrementinputholder clearfix inputholder">' +
    ' <label>Adults (12+yrs)</label>' +
    '<span class="minus button">-</span>' +
    '<input class="incrementinput hotel-adult" type="text" value="0">' +
    '<span class="plus button">+</span>' +
    '</div>' +
    '</div>' +

    '<div class="item children">' +
    '<div class="incrementinputholder clearfix inputholder">' +
    '<label>Children (0-12)</label>' +
    ' <span class="minus button">-</span>' +
    '<input class="incrementinput hotel-child" type="text" value="0">' +
    '<span class="plus button">+</span>' +
    '</div>' +
    '</div>' +

    '<div class="item typeOfRoom">' +
        '<div class="incrementinputholder">' +
        '<label>Type Of Room</label>' +
        '<select class="typeOfRoomSelect">' +
            '<option value="strandart">Standart</option>' +
            '<option value="lux">Lux</option>' +
            '<option value="delux">Deluxe</option>' +
            '<option value="premium">Premium</option>' +
            '<option value="executive">Executive</option>' +
            '<option value="jnr.suite">Jnr.Suite</option>' +
            '<option value="deluxeSuit">Deluxe Suite</option>' +
            '<option value="grandLuxurySuite">Grand Luxury Suite</option>' +
            '<option value="villa">Villa</option>' +
            '<option value="presidentialsuite">Presidential suite</option>' +
        '</select>' +
        '</div>' +
    '</div>' +

    '<div class="item childageholder">' +
    '</div>' +
    '</div>';

$(document).ready(function(){
    // Show count after click ok Travelers field on travelers popup

    $('.people-1, .trav-ok-btn').click(function () {
        $('#hotel-peoplewrap-1').slideToggle();
        $('#hotel-peoplewrap-form').slideToggle();
    });
    $('.people-2, .trav-ok-btn').click(function () {
        $('#hotel-peoplewrap').slideToggle();
        $('#hotel-peoplewrap-form-2').slideToggle();
    });

    $(".paneldata .people").click(function(event) {
        // var adlt = 0;
        // var chld = 0;
        // $(this).next().find('.prowwrap .prow .item .inputholder input.hotel-adult').each(function() {
        //     var type = $(this).attr("type");
        //     if ((type == "text")) {
        //         adlt += parseInt($(this).val());
        //         chld += parseInt($(this).parent().parent().next().find(".hotel-child").val());
        //     }
        // });
        // $(this).text( adlt + " Adults / " +  chld + " Children " );
        var adlt = 0;
        var chld = 0;
        $('#hotel-peoplewrap .prow').each(function()
        {
            adults = $(this).find(".hotel-adult").val();
            children = $(this).find(".hotel-child").val();
            adlt = parseInt(adlt) + parseInt(adults);
            chld = parseInt(chld) + parseInt(children);
        });
        $("#hotels .people").text( adlt + " Adults / " +  chld + " Children " );
        PeoplesToJson()
    });

    // adding room row
    $('.addaroom').click(function(e) {
        e.preventDefault(), $(strroom).appendTo($(this).siblings('.prowwrap'));
    });
    // remove room row
    $('.removeroom').click(function(e) {
        e.preventDefault(), $(this).siblings('.prowwrap').find('.prowdynamic:last-of-type').remove();
    });

    $(document).on('click', '.button', function(event) {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;

        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
            // if ($button.parents('.children').length) {
            //     $($button.parents('.children').next()).find('.incrementinputholder:nth-child(' + oldValue + ')').remove();
            // }
        }

        $button.parent().find("input").attr('value', newVal);
    });
    function PeoplesToJson(){
        var adultChildArr = [];

        $('.prow').each(function()
        {
            self = $(this);

            adultChildArr.push(
                {
                    adult: $(this).find(".hotel-adult").val(),
                    child: $(this).find(".hotel-child").val(),
                    type: $(this).find('.typeOfRoomSelect').val()
                });


        });

        $('.guests').val(JSON.stringify(adultChildArr));
    }

    $(".trav-up").click(function(event) {
        var adlt = 0;
        var chld = 0;
        var message = '';

        var currentRoom = 0;

        $('#hotel-peoplewrap .prow').each(function()
        {
            adults = $(this).find(".hotel-adult").val();
            children = $(this).find(".hotel-child").val();
            typeOfRoom = $(this).find('.typeOfRoomSelect').val();

            adlt = parseInt(adlt) + parseInt(adults);
            chld = parseInt(chld) + parseInt(children);
            currentRoom++;

            message = message + "Room #" + currentRoom + ", Adults " + adults + ", Children " +  children + ", Type of room " + typeOfRoom + '<br>';
        });

        // alert(message);
        $("#your-hidden").text( message );
        $("#hotels .people").text( adlt + " Adults / " +  chld + " Children " );



        // $(this).parent().find('.prowwrap .prow .item .inputholder input.hotel-adult').each(function() {
        //     var type = $(this).attr("type");
        //     if ((type == "text")) {
        //         adlt += parseInt($(this).val());
        //         chld += parseInt($(this).parent().parent().next().find(".hotel-child").val());
        //     }
        // });
        //$(this).parent().parent().find(".people").text( adlt + " Adults / " +  chld + " Children " );
        // $(this).parent().parent().find("#your-hidden").val( adlt + " Adults / " +  chld + " Children " );
        PeoplesToJson()
    });

    $(".trav-down").click(function(event) {
        var adlt = 0;
        var chld = 0;
        var message = '';

        var currentRoom = 0;

        $('#hotel-peoplewrap-form .prow').each(function()
        {
            adults = $(this).find(".hotel-adult").val();
            children = $(this).find(".hotel-child").val();
            typeOfRoom = $(this).find('.typeOfRoomSelect option:selected').text();

            adlt = parseInt(adlt) + parseInt(adults);
            chld = parseInt(chld) + parseInt(children);
            currentRoom++;

            message = message   + "Room #" + currentRoom + ", Adults: " + adults + ", Children: " +  children + ", Type of room: " + typeOfRoom + '\n';
        });

        $("#your-hidden").val(message);
        $("#hotels-down .people").text( adlt + " Adults / " +  chld + " Children " );



        // $(this).parent().find('.prowwrap .prow .item .inputholder input.hotel-adult').each(function() {
        //     var type = $(this).attr("type");
        //     if ((type == "text")) {
        //         adlt += parseInt($(this).val());
        //         chld += parseInt($(this).parent().parent().next().find(".hotel-child").val());
        //     }
        // });
        //$(this).parent().parent().find(".people").text( adlt + " Adults / " +  chld + " Children " );
        // $(this).parent().parent().find("#your-hidden").val( adlt + " Adults / " +  chld + " Children " );
        PeoplesToJson()
    });
})

