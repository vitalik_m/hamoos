$(document).ready(function() {
    $('.back-slider').slick({
        lazyLoad: 'ondemand',
        fade: true,
        autoplay: true,
        autoplaySpeed: 4000,
        pauseOnHover: false



    });

    $('.tour-col').slick({
        autoplay:true,
        autoplaySpeed: 2000,
        pauseOnHover: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3

                }
            }
                ]
    });
    $('.casino-col').slick({
        autoplay:true,
        autoplaySpeed: 2000,
        pauseOnHover: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    $(".gal-item-link").fancybox();
});
