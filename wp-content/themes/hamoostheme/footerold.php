<footer>

    <div class="container-fluid footer">
        <div class="col-md-6 col-md-offset-1 col-xs-12 newsletter">
     
<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html58c29701b5115-1" class="wysija-msg ajax"></div><form id="form-wysija-html58c29701b5115-1" method="post" action="#wysija" class="widget_wysija html_wysija">
<p class="wysija-paragraph">
    
    
    	<input type="email" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Subscribe to get promotions" placeholder="Subscribe to get promotions" value="" />
    
    
    
    <span class="abs-req">
        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
    </span>
    
</p>
<input class="wysija-submit wysija-submit-field" type="submit" value="Subscribe!" />

    <input type="hidden" name="form_id" value="1" />
    <input type="hidden" name="action" value="save" />
    <input type="hidden" name="controller" value="subscribers" />
    <input type="hidden" value="1" name="wysija-page" />

    
        <input type="hidden" name="wysija[user_list][list_ids]" value="1" />
    
 </form></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">

            <?php get_categories(); ?>

            <div class="col-md-2 col-md-offset-1 col-xs-12 weather-time">

                <!-- weather widget start --><a href="//www.booked.net/weather/colombo-19459"><img src="//w.bookcdn.com/weather/picture/32_19459_1_1_95a5a6_250_7f8c8d_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=2&domid=" /></a><!-- weather widget end -->

                <iframe src="http://free.timeanddate.com/clock/i5hzp1e9/n389/fs20/fcfff/tct/pct" frameborder="0" width="162" height="25" allowTransparency="true"></iframe>



            </div>

            <div class="footer-menu col-xs-12 col-md-2">

                <div class="footer-title text-uppercase text-center">

                    menu

                </div>

                <ul class="nav text-uppercase text-center">

                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>about-us/" class="footer-menu-item">about us</a></li>

                    <li><a href="http://blog.hamoos.com/" class="footer-menu-item">Blog</a></li>

                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>gallery/" class="footer-menu-item">gallery</a></li>

                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact-us/" class="footer-menu-item">contact us</a></li>

                </ul>

            </div>

            <div class="col-md-4  col-xs-12">

                <div class="footer-title text-uppercase text-center">

                    contact us

                </div>

                <div class="contact-description">

                    <div class=" col-xs-12">

                        <p class="col-md-6">Address : No 34, D.R. Wijewardena Mawatha,</p>

                        <p class="col-md-6">Colombo 10, Sri Lanka.</p>



                    </div>

                    <div class="clearfix"></div>

                    <div class=" col-xs-12">

                        <p class="col-md-6">24х7 Hot line:<br> + 94 77 8693205<br>

                             +94 11 2334284</p>

                        <p class="col-md-6">E-mail   : info@hamoos.com</br>

				travels@hamoos.com

				</p>

							

                    </div>

                </div>

            </div>

            <div class="col-md-2  col-xs-12 footer-follow">

                <div class="footer-title text-uppercase text-center">

                    follow us on

                </div>

                <div class=" col-md-12 smo footer-smo">

                    <a href="https://www.facebook.com/Hamoos-Travels-LTD-1833061196977399/" class="smo-item facebook"></a>

                    <a href="tel:+94778693205" class="smo-item google"></a>

                    <a href="https://twitter.com/hamoos_travels" class="smo-item twitter"></a>

                    <a href="mailto:info@hamoos.com" class="smo-item mail"></a>

                </div>

            </div>

            <div class="clearfix"></div>

            <div class="sponsors-logo col-md-4 col-md-offset-8 col-xs-12">

                <div class="col-md-4 col-xs-4">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Civil%20Aviation.jpg" class="img-responsive   sponsors-img">

                    </div>

                <div class="col-md-4 col-xs-4">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Tourist%20Board.jpg" class="img-responsive   sponsors-img">

                    </div>

                <div class="col-md-4 col-xs-4">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/SLAITO_LOGO.JPG" class="img-responsive   sponsors-img">

                    </div>

            </div>

            <p class="text-center col-xs-12 copyright">© Hamoos Travels 2012-2016 | Website design and developed by ProITGlobal </p>

        </div>

    </div>



</footer>

<!--    <script src="resources/newfiles/libs/fancybox/jquery.fancybox.js"></script>-->

<!--    <script   src="https://code.jquery.com/jquery-3.1.1.js"   integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>-->

<!--    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

<script src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/js/jquery-3.1.1.min.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/libs/bootstrap/dist/js/bootstrap.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/slick-1.6.0/slick/slick.min.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/js/slider.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/js/index.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/js/room-footer.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/js/room-header.js"></script>











<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/libs/fancybox/jquery.fancybox.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/TimePicki-master/js/timepicki.js"></script>
<script type="text/javascript">
    $("#arriving_date, #depature_date, #transfer_date,#arriving_datec, #depature_datec,#arriveair,#form-arr,#form-dep, #contact-us-arriving-date, #contact-us-departure-date,#departureair").datepicker({
        dateFormat : "dd/mm/yy",
        minDate: 0
    })
    $("#arriving_date, #depature_date, #transfer_date,#arriving_datec, #depature_datec,#arriveair,#form-arr,#form-dep, #contact-us-arriving-date, #contact-us-departure-date, #departureair").on("changeDate", function(ev){
        $(this).datepicker("hide");
    });
    $(".time_element").timepicki({
	show_meridian:false,
	max_hour_value:24
});
</script>
<?php wp_footer(); ?>
</body>
</html>