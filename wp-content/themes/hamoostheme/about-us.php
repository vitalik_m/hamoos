<?php
/*
Template Name: Casinos template
*/
get_header();?>

<main>


    <div class="container-fluid">
        <div class="container pg-container inside-pg-container" id="pg-container">
            <div id="mobile-search-viewer" class="col-xs-12 mobile-search-viewer-nomargin"></div>
            <div class="clearfix"></div>
            <div class="row">
                <article>
                    <div class="col-xs-12 pg-container-inner">
                        <h1 class="col-xs-12"><?php the_title();?></h1>
                        <div class="col-xs-12 body-content">
                          <?php the_post();the_content();?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 t2-wrap">
                            <div class="row">
                                <div class="col-xs-6 t2-wrap-item">
                                    <div class="t2-wrap-item-inner-wrap">
                                        <div class="col-xs-6 t2-wrap-item-img parent">
                                            <?php
                                            echo ' <img class="child" src="' . get_stylesheet_directory_uri() . '/resources/newfiles/new-img/vision.jpg" alt="A boy pausing with a smile from his work station">';
                                            ?>
                                            <!--                                            <img class="child" src="' . get_stylesheet_directory_uri() . '/resources/newfiles/new-img/vision.jpg" alt="A boy pausing with a smile from his work station">-->
                                            <div class="t2-img-arrow"></div>
                                        </div>
                                        <div class="col-xs-6 t2-wrap-item-rgt">
                                            <div class="col-xs-12 t2-wrap-item-title">Vision</div>

                                            <div class="col-xs-12 t2-wrap-item-desc t2-desc-default t2-desc-about">
                                                Our vision is to maintain and continuously improve as a unique travel agency that provides superior personalized services to all our clients. The travel agency is staffed by employees who understand and believe in the concept of excellent customer service.

                                                Hamoos Travels strives to become the leading player in the field of customized travel throughout Sri Lanka. Our dynamic team is continuously developing their skills to ensure our customers' are serviced to their full satisfaction. We strive to provide even better service to our clients.

                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 t2-wrap-item">
                                    <div class="t2-wrap-item-inner-wrap">
                                        <div class="col-xs-6 t2-wrap-item-img parent" style="overflow: hidden; position: relative;">
                                            <?php
                                            echo ' <img class="child" src="' . get_stylesheet_directory_uri() . '/resources/newfiles/new-img/mission.jpg" alt="A boy pausing with a smile from his work station">';
                                            ?>
                                            <!--                                            <img class="child" src="'--><?// echo get_stylesheet_directory_uri(); ?><!-- '/resources/newfiles/new-img/mission.jpg" alt="A boy pausing with a smile from his work station">-->
                                            <div class="t2-img-arrow"></div>
                                        </div>
                                        <div class="col-xs-6 t2-wrap-item-rgt">
                                            <div class="col-xs-12 t2-wrap-item-title">MISSION</div>

                                            <div class="col-xs-12 t2-wrap-item-desc t2-desc-default  t2-desc-about">
                                                Our mission is promotes responsible travel to natural areas that conserves the environment, respecting and benefiting local people. We believe that sustainable, sensitive tourism is an invaluable help to developing the communities and preserving the environments of Sri Lanka. Our aim to encourage more people to visit Sri Lanka by promoting supremely enjoyable and informative trips whilst maximizing the benefits these generate for the local economies, environments and host communities in Sri Lanka
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="clearfix"></div>
                            </div>
                        </div>

                    </div>


                </article>



            </div> <!-- END :: row -->
        </div> <!-- END :: container -->
    </div>



</main>


<?php get_footer();?>
