<?php
/*
Template Name: Services template
*/
get_header();?>
<main>
    <div class="container-fluid">
        <?php while ( have_posts() ){ the_post();
            echo ' <h1 class="text-uppercase col-xs-12 text-center">';
            the_title();
            echo '</h1><div class="col-xs-12 body-content">';
            the_content();
            echo '</div>';
        }?>
        <div class="col-xs-12 t2-wrap">
            <div class="row">
                <?php $curtitle=get_the_title();
//                $the_query = new WP_Query( 'tag='. $curtitle );
                // 1 значение по умолчанию
                $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
                $the_query = new WP_Query( array(
                    'posts_per_page' => 12,
                    'tag'  => $curtitle,
                    'paged'          => $paged,
                ) );
                 while  ($the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="col-sm-6 col-xs-12 t2-wrap-item">
                        <div class="t2-wrap-item-inner-wrap">
                            <div class="col-xs-6 t2-wrap-item-img parent" >

                                <?php if ( has_post_thumbnail()) { ?>
					<?php the_post_thumbnail(); ?>
                                    /*<a href="#" title="<?php the_title_attribute(); ?>" >
                                        
                                    </a>*/
                                <?php } ?>
                            </div>
                            <div class="col-xs-6 t2-wrap-item-rgt services-item">
                                <div class="col-xs-12 t2-wrap-item-title"><?php the_title(); ?></div>
                                <div class="col-xs-12 t2-wrap-item-desc services-desc"><?php the_content(); ?></div>
				 <?php if ( has_tag('promotions')) { ?>
					<div class="col-xs-12 t2-wrap-item-link">
                                    		 <a href="<?php the_permalink(); ?>" title="" class="find-out-more hotel-inquire" data-id="0">inquire</a>
                                	</div>
				<?php } ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata();
                $big = 999999999; // уникальное число

                ?>
            </div>

        </div>

    </div>
    <div class="pagination">


    <?php echo paginate_links( array(
        'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format'  => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total'   => $the_query->max_num_pages
    ) );?>
    </div>
</main>

<?php  get_footer(); ?>
