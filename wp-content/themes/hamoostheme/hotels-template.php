<?php
/*
Template Name: Hotels template
*/
get_header();?>
    <main>
        <div class="container-fluid">
            <div class="col-xs-12 t2-wrap">
                <div class="row">
                    <?php while ( have_posts() ){ the_post();
                        echo ' <h1 class="text-uppercase col-xs-12 text-center">';
                        the_title();
                        echo '</h1>';
                    }?>
			<div class="for-go-back">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="go-back">Go Back</a>
			</div>
                    <?php  $the_query = new WP_Query('tag=hotels&posts_per_page=12'); ?>
                    <?php while  ($the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="col-sm-6 col-xs-12 t2-wrap-item">
                            <div class="t2-wrap-item-inner-wrap">
                                <div class="col-xs-6 t2-wrap-item-img parent" >
                                    <?php if ( has_post_thumbnail()) { ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                            <?php the_post_thumbnail(); ?>
                                        </a>
                                    <?php } ?>
                                </div>
                                <div class="col-xs-6 t2-wrap-item-rgt">
                                    <div class="col-xs-12 t2-wrap-item-title"><?php the_title(); ?></div>
                                    <div class="col-xs-12 t2-wrap-item-desc services-desc"><?php the_content(); ?></div>
                                    <div class="col-xs-12 t2-wrap-item-link">
                                        <a href="#inquire-hotels" title="<?php strtolower(the_title()); ?>" class="find-out-more hotel-inquire" data-id="0">inquire</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata();?>
                </div>
            </div>
            <div class="col-xs-12 body-content" id="inquire">
              <div id="inquire-hotels"></div>
                <h1 class="col-xs-12 text-center text-uppercase" >inquire</h1>
				<div class="hotelsdown testTest" style="display:none;">
                                            <div class="prow prowdynamic clearfix">
                                                <div class="item adultsNumber">
                                                    <div class="incrementinputholder clearfix inputholder">
                                                        <label>Adults (12+yrs)</label>
                                                        <span class="minus button">-</span>
                                                        <input class="incrementinput hotel-adult" type="text" value="2">
                                                        <span class="plus button">+</span>
                                                    </div>
                                                </div>
                                                <div class="item children">
                                                    <div class="incrementinputholder clearfix inputholder">
                                                        <label>Children (0-12)</label>
                                                        <span class="minus button">-</span>
                                                        <input class="incrementinput hotel-child" type="text" value="0">
                                                        <span class="plus button">+</span>
                                                    </div>
                                                </div>
                                                <div class="item typeOfRoom ">
                                                    <div class="incrementinputholder">
                                                    <label>Type Of Room</label>
                                                    <select class="typeOfRoomSelect">
                                                        <option value="strandart">Standart</option>
                                                        <option value="lux">Lux</option>
                                                        <option value="delux">Deluxe</option>
							<option value="premium">Premium</option>
                                                        <option value="executive">Executive</option>
							<option value="jnr.suite">Jnr.Suite</option>
                                                        <option value="deluxeSuit">Deluxe Suite</option>
							<option value="grandLuxurySuite">Grand Luxury Suite</option>
                                                        <option value="villa">Villa</option>
							<option value="presidentialsuite">Presidential suite</option>
                                                    </select>
                                                    </div>
                                                </div>
					</div>
				</div>
                <p>Fill in all the fields below as clearly as possible to ensure that we get a better idea of your needs and thus can serve you better.</p>
                <div class="form-wrap">
                    <?php the_content() ?>
                </div>
                <p> * marked fields are mandatory </p>
            </div>
        </div>
    </main>

<script type="text/javascript">
    $( document ).ready(function() {
 	$("#hotelsname").val('<?php echo ucwords($_GET['hotel']); ?>');
        $("#hotelsadults").val('<?php echo $_GET['adults']; ?>');
        $("#hotelsnat").val('<?php echo ucwords($_GET['country']); ?>');
        $("#hotelschild").val('<?php echo ucfirst($_GET['children']); ?>');

	        var fullAddedHtml = '';
        var chld = 0;
        var adlt = 0;
        var hotelType = 'Standart',
            guests = {};



//если $_GET['guests'] - пустой - то fullAddedHtml + $($('.testTest')[0]).children()[0].outerHTML;

//иначе код снизу



	 var getguest =  '<?php echo $_GET['guests']; ?>';



   	 if (getguest == ""){

     	   $("#hotels-down .people").text( adlt + " Adults / " +  chld + " Children " );

   	 }

  	 else{

         guests = JSON.parse(getguest);
         console.log(guests);

   	 }

	$.each(guests, function(idx, obj) {
		hotelType = obj.type;
		adlt = parseInt(adlt) + parseInt(obj.adult);
		chld = parseInt(chld) + parseInt(obj.child);

		$('.testTest .hotel-adult').attr('value',obj.adult);
		$('.testTest .hotel-child').attr('value',obj.child);
		$('.testTest .typeOfRoomSelect option').removeAttr('selected');
	        $('.testTest .typeOfRoomSelect option[value="' + obj.type + '"]').attr('selected','selected');

		//$('.testTest .typeOfRoomSelect').val("lux").attr("selected", "selected");

		fullAddedHtml = fullAddedHtml + $($('.testTest')[0]).children()[0].outerHTML;
	});	

	$(fullAddedHtml).appendTo('.prowwrap');	

	$("#hotels-down .people").text( adlt + " Adults / " +  chld + " Children " );

    });

</script>

<?php

get_footer();?>