<?php
/*
Template Name: Tours template
*/

get_header(); ?>



<main>
    <div class="container-fluid">
  <div class="col-xs-12 t2-wrap">
    <div class="row">

      <div class="col-sm-6 col-xs-12 t2-wrap-item">
        <div class="t2-wrap-item-inner-wrap">
          <div class="col-xs-6 t2-wrap-item-img parent" style="overflow: hidden; position: relative;">

            <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/dinner-650.jpg" alt="Outdoor dining at Cinnamon Lodge Habarana while enjoying the romantic sunset" >
            <div class="t2-img-arrow"></div>
          </div>
          <div class="col-xs-6 t2-wrap-item-rgt">

            <div class="col-xs-12">
              <div class="col-xs-12 t2-wrap-item-highlighter t2-most-pop">most<br>popular<br>tour</div>
            </div>

            <div class="col-xs-12 t2-wrap-item-title t2-most-pop-title">Bespoke Tours</div>


          </div>
        </div>
      </div>
        <?php
        $args=array(
            'orderby' => 'name',
            'order' => 'DESC',
            'hide_empty' => '0',
            'exclude' => "1"
        );
        $categories=get_categories($args);
        $siteurl = site_url();



        foreach ($categories as $category1) {
            if($category1->category_parent == 0) {

                $cat_data = get_option("category_{$category1->term_id}");
                if (!empty($cat_data['price'])){
                    $price_html=$cat_data['price'];
                }else{
                    $price_html='Price on request';
                }

                if (function_exists('category_image_src')) {
                    $category_image = category_image_src( array( 'size' => 'full' ) , false );
                } else {
                    $category_image = '';
                }
                echo '<div class="col-sm-6 col-xs-12 t2-wrap-item">';
                echo '<div class="t2-wrap-item-inner-wrap">';
                echo '<div class="col-xs-6 t2-wrap-item-img parent img-block-cover" >';
                echo do_shortcode(sprintf('[wp_custom_image_category term_id="%s"]',$category1->term_id));
//                echo '<img class="child" src="' .$siteurl.'/wp-content/uploads/' . $category1->slug . '-cats.jpg" alt="' . $category1->cat_name . '" />';
                echo '<div class="t2-img-arrow"></div></div>';
                echo '<div class="col-xs-6 t2-wrap-item-rgt">';
                echo '<div class="col-xs-12 t2-wrap-item-title">' .  $category1->cat_name . '</div>';
                echo '<div class="col-xs-12">
                        <div class="col-xs-12 t2-wrap-item-highlighter ">'.$price_html.'</div>
                      </div>            
                      <div class="col-xs-12 t2-wrap-item-desc">' . $category1->description . ' </div> 
                      <div class="col-xs-12 t2-wrap-item-link" ><a title="View '.$category1->cat_name . ' in Sri Lanka"  href="' . get_category_link( $category1->term_id ) . '" class="find-out-more">find out more</a></div> </div>';
                echo '</div></div>';
            }
        }
        ?>
                </div>
            </div>
        </div>
</main>


<?php  get_footer(); ?>