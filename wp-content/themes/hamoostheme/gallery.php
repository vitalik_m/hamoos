<?php
/*
Template Name: Gallery template
*/
get_header();?>

<main>
    <div class="container-fluid">
    <div class="col-xs-12 body-content">
        <div id="gal-cat-anchor"></div>
        <div class="col-xs-12 gal-cat-mobile-drop fold">culture</div>

        <div class="col-xs-12 gal-cat-wrap">
            <div class="col-xs-2 gal-cat-item"> <a href="#gal-cat-anchor" data-id="0" title="" id="cat0" class="selected">culture</a> </div>
            <div class="col-xs-2 gal-cat-item"> <a href="#gal-cat-anchor" data-id="1" title="" id="cat1" class="">wild life</a> </div>
            <div class="col-xs-2 gal-cat-item"> <a href="#gal-cat-anchor" data-id="2" title="" id="cat2" class="">beaches</a> </div>
            <div class="col-xs-2 gal-cat-item"> <a href="#gal-cat-anchor" data-id="3" title="" id="cat3" class="">lifestyle</a> </div>
            <div class="col-xs-2 gal-cat-item"> <a href="#gal-cat-anchor" data-id="4" title="" id="cat4" class="">adventure</a> </div>
            <div class="col-xs-2 gal-cat-item"> <a href="#gal-cat-anchor" data-id="5" title="" id="cat5" class="">others</a> </div>
        </div>

        <div class="col-xs-12 gal-item-wrap">
            <div class="gal-item-group gal-group-0" style="display: block;">
                <div class="col-xs-3 gal-item gal-item-default" style="background:#fedc00;">
                    <span>Stepping <br>Back<br> in Time</span>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/ancient-1.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/ancient-1.jpg" alt=" Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/ancient-2.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/ancient-2.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/buddha-1.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/buddha-1.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/buddha-2.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/buddha-2.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/buddhism.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/buddhism.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/building.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/building.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/parade.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/parade.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/shrine.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/shrine.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/sri-lanka-1.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/sri-lanka-1.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/sri-lanka.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/sri-lanka.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>
                <div style="background: rgb(254, 220, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-0">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/temple.jpg" data-fancybox-group="gallery0" title="sri lanca culture">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/culture/temple.jpg" alt="Sri Lankan Culture" style=" top: 0px; left: -8px;">
                    </a>
                </div>


            </div>
            <div class="gal-item-group gal-group-1" style="display: none;">
                <div class="col-xs-3 gal-item gal-item-default" style="background:#008ffe;">
                    <span>Journeys <br>into<br> the Wild </span>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild1.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild1.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild2.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild2.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild3.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild3.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild4.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild4.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild5.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild5.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild6.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild6.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild7.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild7.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild8.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild8.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild9.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild9.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild10.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild10.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(0, 143, 254); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-1">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild11.jpg" data-fancybox-group="gallery1" title=" Sri Lanka's wildlife">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/wild_life/wild11.jpg" alt="Sri Lanka's wildlife" style=" top: 0px; left: 0px;">
                    </a>
                </div>
            </div>
            <div class="gal-item-group gal-group-2" style="display: none;">
                <div class="col-xs-3 gal-item gal-item-default" style="background:#fe4800;">
                    <span>Sun <br>Drenched <br>Sanctuaries</span>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach1.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach1.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach2.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach2.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach3.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach3.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach4.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach4.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach5.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach5.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach6.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach6.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach7.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach7.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach8.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach8.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach9.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach9.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach10.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach10.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(254, 72, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-2">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach11.jpg" data-fancybox-group="gallery2" title="beaches_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/beaches/beach11.jpg" alt="beaches_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>

            </div>
            <div class="gal-item-group gal-group-3" style="display: none;">
                <div class="col-xs-3 gal-item gal-item-default" style="background:#8ad200;"> <span>Multicultural Diversity</span> </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle1.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle1.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle2.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle2.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle3.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle3.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle4.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle4.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle5.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle5.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle6.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle6.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle7.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle7.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle8.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle8.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle9.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle9.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle10.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle10.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(138, 210, 0); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-3">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle11.jpg" data-fancybox-group="gallery3" title="srilankan_lifestyle">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/lifestyle/lifestyle11.jpg" alt="srilankan_lifestyle" style=" top: 0px; left: 0px;">
                    </a>
                </div>

            </div>
            <div class="gal-item-group gal-group-4" style="display: none;">
                <div class="col-xs-3 gal-item gal-item-default" style="background:#9901d2;">
                    <span>Adventures <br>in <br>Paradise</span>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure1.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure1.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure2.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure2.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure3.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure3.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure4.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure4.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure5.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure5.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure6.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure6.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure7.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure7.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure8.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure8.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure9.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure9.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure10.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure10.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(153, 1, 210); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-4">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure11.jpg" data-fancybox-group="gallery4" title="adventures_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/adventure/adventure11.jpg" alt="adventures_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
            </div>
            <div class="gal-item-group gal-group-5" style="display: none;">
                <div class="col-xs-3 gal-item gal-item-default" style="background:#01d2a3;"> <span>Shades <br>of <br>Sri Lanka</span> </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others1.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others1.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others2.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others2.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others3.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others3.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others4.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others4.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others5.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others5.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others6.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others6.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others7.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others7.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others8.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others8.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others9.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others9.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                        <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others10.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others10.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
                <div style="background: rgb(1, 210, 163); overflow: hidden; position: relative;" class="parent col-xs-3 gal-item gal-item-5">
                    <a rel="alternate" class="gal-item-link" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others11.jpg" data-fancybox-group="gallery5" title="visit_srilanka">
                        <img class="child" src="<?php echo get_stylesheet_directory_uri(); ?>/resources/newfiles/new-img/others/others11.jpg" alt="visit_srilanka" style=" top: 0px; left: 0px;">
                    </a>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>


<?php

get_footer();?>
?>