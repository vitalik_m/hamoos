<?php
/*
Template Name: Contact us template
*/
get_header();?>
<main>
    <div class="container-fluid">
        <div class="container pg-container inside-pg-container" id="pg-container">
            <div id="mobile-search-viewer" class="col-xs-12 mobile-search-viewer-nomargin"></div>
            <div class="clearfix"></div>
            <div class="row" id="enquire-trans">
                <article>
                    <div class="col-xs-12 pg-container-inner contact-us-title">
                        <h1 class="text-center" id="inquire-hotels"><?php the_title(); ?></h1>
                        <div class="col-xs-12 body-content" >
                            <p><?php the_post();the_content(); ?></p>
                            <p style="margin-left: 1px"> * marked fields are mandatory </p>
                        </div>


                        </div>




                    
                </article>

            </div> <!-- END :: row -->
        </div> <!-- END :: container -->
    </div>
</main>

<?php $getcartype=$_GET['car_type'];?>

    <script type="text/javascript">

        $( document ).ready(function() {
//        alert('<?php //echo $getadults; ?>//');
            $("#cartypetransfers").val('<?php echo ucwords($_GET['car_type']); ?>');
            $("input:radio[name=radio-826]").filter('[value="<?php echo ucwords($_GET['dropType']); ?>"]').prop('checked', true);
        });
    </script>

<? get_footer();?>